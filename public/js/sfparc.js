function ajax_load(_url, _update, _data = null) {
    $('#chargement').show();
    $.ajax({
        method: "GET",
        url: _url,
        data: _data
    })
            .done(function (data) {
                $(_update).html(data);
            })
            .fail(function (data) {
                //$(_update).html(data.responseText);
                bootbox.alert({
                    size: "small",
                    title: "<span class='text-warning'> " + status + "</span>",
                    message: data.responseText,
                    buttons: {
                        ok: {
                            className: "btn-outline-warning"
                        }
                    }
                });
            })
            .always(function () {
                $('#chargement').hide();
            });
}
function ajax_form(_url, _update, _form) {
    $('#chargement').show();
    $.post(
        _url,
        $(_form).serialize()
        )
        .done(function (data) {
            $(_update).html(data);
        })
        .fail(function (data, status) {
            //$(_update).html(data.responseText);
            bootbox.alert({
                size: "small",
                title: "<span class='text-warning'> " + status + "</span>",
                message: data.responseText,
                buttons: {
                    ok: {
                        className: "btn-outline-warning"
                    }
                }
            });
        })
        .always(function () {
            $('#chargement').hide();
        });
}
function listeDetail(_obj, _item, _display) {
    if (!_item.attr('id')) {
        _id = '';
        _header = _item.html();
        _libelle = '';
        _rchgmt = 0;
    } else {
        _id = _item.attr('id');
        _header = _obj + " : " + _item.attr('id')
        _libelle = _item.find('span').html();
        //_rchgmt = _item.attr('data-rchgmt');
    }
    $('#id-' + _obj).val(_id);
    $('#code-' + _obj).html(_header);
    $('#libelle-' + _obj).val(_libelle);
    if (_display === true) {
        $('#group-id-' + _obj).removeClass('d-none');
        $('#new-' + _obj).val(1);
    } else {
        $('#group-id-' + _obj).addClass('d-none');
        $('#new-' + _obj).val(0);
    }
    /*    $('#rchrgmt-'+obj).val(_rchgmt);
     if( $('#rchrgmt-'+obj).val() == 1 ){
     $('#rchrgmt-' + obj).parent().addClass('active');
     } else {
     $('#rchrgmt-' + obj).parent().removeClass('active');
     }*/
}
function confirm(_id, _params, _setNull = false) {
    _titre = $(this).attr('title');
    _message = $(this).attr('data-confirm');
    _target = $(this).attr('data-target');
    _url = $(this).attr('url');
    bootbox.confirm({
        size: "small",
        title: _titre,
        message: '<p class="text-justify">' + _message + '</p>',
        buttons: {
            confirm: {
                label: 'Supprimer',
                className: 'btn-outline-danger btn-action remove'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-outline-secondary btn-action cancel'
            }
        },
        callback: function (result) {
            if (result === true) {
                $('#chargement').show();
                $.ajax({
                    method: "GET",
                    url: _url,
                    data: _params
                })
                        .done(function (data) {
                            bootbox.alert({
                                size: "small",
                                title: '<span class="text-success">Suppression</span>',
                                message: '<p class="text-justify text-success">' + data + '</p>',
                                buttons: {
                                    ok: {
                                        className: "btn-outline-success"
                                    }
                                }
                            });
                            if (_setNull) {
                                $(_target).html('');
                            } else {
                                //$("#" + _target).html(data);
                            }
                            $("#" + _id).remove();
                        })
                        .fail(function (data, status) {
                            bootbox.alert({
                                size: "small",
                                title: "<span class='text-warning'>Suppression : " + status + "</span>",
                                message: data.responseText,
                                buttons: {
                                    ok: {
                                        className: "btn-outline-warning"
                                    }
                                }
                            });
                        })
                        .always(function () {
                            $('#chargement').hide();
                        });
            }
        }
    });
}

(function ($) {
    // custom css expression for a case-insensitive contains()
    jQuery.expr[':'].Contains = function (a, i, m) {
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };
}(jQuery));

function listFilter(_header, _list) { // header is any element, list is an unordered list
    var _input = $(_header).find('[type=search]');
    $(_input)
            .change(function () {
                var _filter = $(this).val();
                if (_filter) {
                    // this finds all links in a list that contain the input,
                    // and hide the ones not containing the input while showing the ones that do
                    $(_list).find(".search-content:not(:Contains(" + _filter + "))").parents('li').slideUp();
                    $(_list).find(".search-content:Contains(" + _filter + ")").parents('li').slideDown();
                } else {
                    $(_list).find("li").slideDown();
                }
                return false;
            })
            .keyup(function () {
                // fire the above change event after every letter
                $(this).change();
            });
}

function upperCaseF(a) {
    setTimeout(function () {
        a.value = a.value.toUpperCase();
    }, 1);
}