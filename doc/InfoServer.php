<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoServer
 *
 * @ORM\Table(name="info_server", indexes={@ORM\Index(name="FI_ERVER_VERSION", columns={"os_version"}), @ORM\Index(name="FK_APPLIANCE_SERVER", columns={"apl_id"}), @ORM\Index(name="IDX_OS_SERVER", columns={"os_id", "os_version", "os_sp"}), @ORM\Index(name="FI_ERVER_SP", columns={"os_sp"}), @ORM\Index(name="IDX_97B08AA03DCA04D1", columns={"os_id"})})
 * @ORM\Entity
 */
class InfoServer
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="srv_domain", type="string", length=50, nullable=true, options={"comment"="Network domain or Workgroup"})
     */
    private $srvDomain;

    /**
     * @var string|null
     *
     * @ORM\Column(name="srv_ip", type="string", length=16, nullable=true, options={"comment"="server IP Address"})
     */
    private $srvIp;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="srv_auto_update", type="boolean", nullable=true, options={"default"="1","comment"="To be updated automatically"})
     */
    private $srvAutoUpdate = '1';

    /**
     * @var \Appliance
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Appliance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apl_id", referencedColumnName="apl_id")
     * })
     */
    private $apl;

    /**
     * @var \Software
     *
     * @ORM\ManyToOne(targetEntity="Software")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="os_id", referencedColumnName="soft_id")
     * })
     */
    private $os;

    /**
     * @var \Software
     *
     * @ORM\ManyToOne(targetEntity="Software")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="os_sp", referencedColumnName="soft_id")
     * })
     */
    private $osSp;

    /**
     * @var \Software
     *
     * @ORM\ManyToOne(targetEntity="Software")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="os_version", referencedColumnName="soft_id")
     * })
     */
    private $osVersion;


}
