<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoRouter
 *
 * @ORM\Table(name="info_router", indexes={@ORM\Index(name="FK_APPLIANCE_ROUTER", columns={"apl_id"})})
 * @ORM\Entity
 */
class InfoRouter
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="rtr_ip", type="string", length=16, nullable=true, options={"comment"="router IP Address"})
     */
    private $rtrIp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rtr_mac", type="string", length=255, nullable=true, options={"comment"="router MAC Address"})
     */
    private $rtrMac;

    /**
     * @var \Appliance
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Appliance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apl_id", referencedColumnName="apl_id")
     * })
     */
    private $apl;


}
