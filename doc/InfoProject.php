<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoProject
 *
 * @ORM\Table(name="info_project", indexes={@ORM\Index(name="FK_PROJECT_SOFTWARE", columns={"proj_id"}), @ORM\Index(name="FK_PROJECT_STATUS", columns={"status_id"}), @ORM\Index(name="ID_PROJ_PUBLIC", columns={"proj_ispublic"}), @ORM\Index(name="FI_ROJECT_EMAIL", columns={"email_id"})})
 * @ORM\Entity
 */
class InfoProject
{
    /**
     * @var bool|null
     *
     * @ORM\Column(name="proj_ispublic", type="boolean", nullable=true)
     */
    private $projIspublic;

    /**
     * @var \Email
     *
     * @ORM\ManyToOne(targetEntity="Email")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email_id", referencedColumnName="email_id")
     * })
     */
    private $email;

    /**
     * @var \Software
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Software")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="proj_id", referencedColumnName="soft_id")
     * })
     */
    private $proj;

    /**
     * @var \Property
     *
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="prop_id")
     * })
     */
    private $status;


}
