
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- sync_server
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sync_server`;


CREATE TABLE `sync_server`
(
	`srv_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`srv_class` VARCHAR(50) COMMENT 'Server class (ocsServer, LdapServer ...)',
	`srv_name` VARCHAR(30) COMMENT 'Server DNS Name',
	`srv_ip` VARCHAR(16) COMMENT 'Server IP address',
	`srv_desc` VARCHAR(30) COMMENT 'Server description',
	`srv_checksum` INTEGER,
	`srv_freq` VARCHAR(100) COMMENT 'auto refresh frequency (cron format)',
	`srv_url` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`sync_at` DATETIME,
	PRIMARY KEY (`srv_id`),
	UNIQUE KEY `IDX_SYNC_SERVER` (`srv_id`, `srv_class`),
	KEY `ID_SERVER_CLASS`(`srv_class`)
);

#-----------------------------------------------------------------------------
#-- sync_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sync_link`;


CREATE TABLE `sync_link`
(
	`srv_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`srv_class` VARCHAR(50) COMMENT 'Server class (ocsServer, LdapServer ...)',
	`link_id` INTEGER  NOT NULL COMMENT 'linked class id',
	`link_class` VARCHAR(50)  NOT NULL COMMENT 'link class',
	`link_ref` VARCHAR(255)  NOT NULL,
	`created_at` DATETIME COMMENT 'Creation date',
	`updated_at` DATETIME COMMENT 'date of update',
	PRIMARY KEY (`srv_id`,`link_id`,`link_class`),
	KEY `R_LINK_SERVER`(`srv_id`, `srv_class`),
	KEY `R_LINK_CLASS`(`link_id`, `link_class`),
	KEY `ID_LINK_REF`(`link_ref`),
	CONSTRAINT `R_LINK_SERVER`
		FOREIGN KEY (`srv_id`,`srv_class`)
		REFERENCES `sync_server` (`srv_id`,`srv_class`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- property
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `property`;


CREATE TABLE `property`
(
	`prop_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`prop_class` VARCHAR(50) COMMENT 'Propety Appliance class (computer, type, status...)',
	`prop_order` TINYINT,
	`prop_css` VARCHAR(50),
	`prop_img` VARCHAR(50),
	`is_public` TINYINT,
	`prop_color` VARCHAR(8),
	`prop_textcolor` VARCHAR(8),
	`unit_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`prop_id`),
	INDEX `property_FI_1` (`unit_id`),
	CONSTRAINT `property_FK_1`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- property_label
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `property_label`;


CREATE TABLE `property_label`
(
	`prop_id` INTEGER  NOT NULL,
	`lang_id` VARCHAR(2)  NOT NULL,
	`prop_label` VARCHAR(50)  NOT NULL,
	`prop_comment` TEXT,
	`prop_trans` TINYINT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`prop_id`,`lang_id`),
	KEY `R_PROPERTY`(`prop_id`),
	CONSTRAINT `R_PROPERTY_LABEL`
		FOREIGN KEY (`prop_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- info_user
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_user`;


CREATE TABLE `info_user`
(
	`user_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`sfguard_id` INTEGER,
	`unit_id` INTEGER COMMENT 'Unit Id',
	`location_id` INTEGER COMMENT 'Location id',
	`user_surname` VARCHAR(30),
	`user_firstname` VARCHAR(30),
	`user_culture` VARCHAR(5) COMMENT 'Country and Language',
	`user_email` VARCHAR(126) COMMENT 'email address',
	`user_phone` VARCHAR(24) COMMENT 'Office Phone',
	`user_ext` VARCHAR(24) COMMENT 'Phone Extension',
	`user_mobile` VARCHAR(24) COMMENT 'Mobile Phone',
	`user_img` VARCHAR(24) COMMENT 'Image',
	`user_sign` TEXT COMMENT 'Signature on email',
	`user_notes` TEXT,
	`user_staff` TINYINT COMMENT 'user part of the staff',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`user_id`),
	UNIQUE KEY `IDX_USER_EMAIL` (`user_email`),
	KEY `R_USER_SFGUARD`(`sfguard_id`),
	KEY `R_USER_UNIT`(`unit_id`),
	KEY `R_USER_LOCATION`(`location_id`),
	CONSTRAINT `info_user_FK_1`
		FOREIGN KEY (`sfguard_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT,
	CONSTRAINT `info_user_FK_2`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `FK_USER_LOCATION`
		FOREIGN KEY (`location_id`)
		REFERENCES `location` (`location_id`)
		ON DELETE RESTRICT
) COMMENT='Account information';

#-----------------------------------------------------------------------------
#-- user_preference
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `user_preference`;


CREATE TABLE `user_preference`
(
	`user_id` INTEGER  NOT NULL,
	`key` VARCHAR(20)  NOT NULL,
	`value` VARCHAR(20)  NOT NULL,
	`order` INTEGER,
	PRIMARY KEY (`user_id`,`key`,`value`),
	KEY `R_USER_PREFERENCE`(`user_id`),
	CONSTRAINT `user_preference_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- org_unit
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `org_unit`;


CREATE TABLE `org_unit`
(
	`unit_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`cie_id` INTEGER,
	`unit_name` VARCHAR(30)  NOT NULL COMMENT 'Name',
	`parent_id` INTEGER,
	`tree_left` INTEGER COMMENT 'Model column holding nested set left value for a row (behavior ActAsNestedSet)',
	`tree_right` INTEGER COMMENT 'Model column holding nested set right value for a row (behavior ActAsNestedSet)',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`unit_id`),
	KEY `R_UNIT_COMPANY`(`cie_id`),
	KEY `R_UNIT_PARENT`(`parent_id`),
	CONSTRAINT `FK_UNIT_COMPANY`
		FOREIGN KEY (`cie_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `FK_UNIT_DEPENDANCY`
		FOREIGN KEY (`parent_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE CASCADE
) COMMENT='Organisation Unit';

#-----------------------------------------------------------------------------
#-- location
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `location`;


CREATE TABLE `location`
(
	`location_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`unit_id` INTEGER,
	`location_name` VARCHAR(30)  NOT NULL COMMENT 'Name',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`location_id`),
	UNIQUE KEY `IDX_LOCATION` (`unit_id`, `location_id`),
	KEY `R_LOCATION_UNIT`(`unit_id`),
	CONSTRAINT `FK_LOCATION_UNIT`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE CASCADE
) COMMENT='Location';

#-----------------------------------------------------------------------------
#-- company
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `company`;


CREATE TABLE `company`
(
	`cie_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`parent_id` INTEGER,
	`cie_name` VARCHAR(255),
	`cie_website` VARCHAR(255),
	`cie_note` TEXT,
	`created_at` DATETIME,
	`updated_at` DATETIME COMMENT 'Modification date',
	`deleted_at` DATETIME,
	PRIMARY KEY (`cie_id`),
	KEY `R_COMPANY_PARENT`(`parent_id`),
	CONSTRAINT `R_COMPANY_PARENT`
		FOREIGN KEY (`parent_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE SET NULL
);

#-----------------------------------------------------------------------------
#-- cie_type
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `cie_type`;


CREATE TABLE `cie_type`
(
	`cie_id` INTEGER  NOT NULL,
	`cie_type` CHAR(1)  NOT NULL,
	PRIMARY KEY (`cie_id`,`cie_type`),
	CONSTRAINT `R_COMPANY_TYPE`
		FOREIGN KEY (`cie_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- filter
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `filter`;


CREATE TABLE `filter`
(
	`filter_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_id` INTEGER COMMENT 'user the entering',
	`filter_form` VARCHAR(30) COMMENT 'Object referencing the filter',
	`filter_label` VARCHAR(20) COMMENT 'filter label',
	`filter_values` TEXT COMMENT 'Filter actual values',
	PRIMARY KEY (`filter_id`),
	KEY `FK_FILTER_USER`(`user_id`),
	KEY `ID_FILTER_FORM`(`filter_form`),
	CONSTRAINT `R_FILTER_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE CASCADE
);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
