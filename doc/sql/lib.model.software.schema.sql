
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- platform
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `platform`;


CREATE TABLE `platform`
(
	`plf_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`plf_name` VARCHAR(20),
	PRIMARY KEY (`plf_id`)
) COMMENT='Hardware platform';

#-----------------------------------------------------------------------------
#-- software
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `software`;


CREATE TABLE `software`
(
	`soft_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`soft_type` CHAR(1)  NOT NULL,
	`plf_id` INTEGER,
	`soft_name` VARCHAR(255),
	`soft_note` TEXT,
	`editor_id` INTEGER,
	`cat_id` INTEGER,
	`soft_port` INTEGER COMMENT 'Daemon/application default communication port',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`soft_id`),
	KEY `FK_SOFT_PLF`(`plf_id`),
	KEY `FK_SOFT_EDITOR`(`editor_id`),
	KEY `FK_SOFT_CAT`(`cat_id`),
	CONSTRAINT `R_SOFTWARE_PLATFORM`
		FOREIGN KEY (`plf_id`)
		REFERENCES `platform` (`plf_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_SOFTWARE_EDITOR`
		FOREIGN KEY (`editor_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_SOFTWARE_CATEGRORY`
		FOREIGN KEY (`cat_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE SET NULL
);

#-----------------------------------------------------------------------------
#-- info_os
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_os`;


CREATE TABLE `info_os`
(
	`soft_id` INTEGER  NOT NULL,
	`os_id` INTEGER,
	PRIMARY KEY (`soft_id`),
	KEY `FK_OS_SOFTWARE`(`soft_id`),
	KEY `FK_SOFT_OS`(`os_id`),
	CONSTRAINT `R_OS_SOFTWARE`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_SOFT_OS`
		FOREIGN KEY (`os_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- info_version
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_version`;


CREATE TABLE `info_version`
(
	`soft_id` INTEGER  NOT NULL,
	`version_id` INTEGER  NOT NULL COMMENT 'Software version',
	`version_release` DATE COMMENT 'Date on which the version was actually released',
	`version_planned` DATE COMMENT 'Date on which the version is normally due',
	PRIMARY KEY (`soft_id`,`version_id`),
	KEY `FK_SOFTWARE_VERSION`(`soft_id`),
	KEY `FK_VERSION_SOFTWARE`(`version_id`),
	CONSTRAINT `R_SOFTWARE_VERSION`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_VERSION_SOFTWARE`
		FOREIGN KEY (`version_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- license
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `license`;


CREATE TABLE `license`
(
	`lic_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`soft_id` INTEGER  NOT NULL,
	`lic_sn` VARCHAR(255),
	`lic_expiry_date` DATE,
	`apl_id` INTEGER,
	`lic_type` CHAR(1) COMMENT 'License Type (Open source, OEM, Freeware, editor)',
	`lic_free` CHAR(1) default '0',
	`lic_note` TEXT,
	PRIMARY KEY (`lic_id`),
	KEY `FK_SOFT_LICENSE`(`soft_id`),
	KEY `FK_COMPUTER_LICENSE`(`apl_id`),
	KEY `ID_SERIAL`(`lic_sn`),
	KEY `ID_EXPIRATION`(`lic_expiry_date`),
	CONSTRAINT `R_LICENSE_SOFTWARE`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_LICENSE_APPLIANCE`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- computer_software
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `computer_software`;


CREATE TABLE `computer_software`
(
	`soft_id` INTEGER  NOT NULL,
	`apl_id` INTEGER  NOT NULL,
	`lic_id` INTEGER,
	`impl_port` INTEGER COMMENT 'Daemon/application communication port',
	PRIMARY KEY (`soft_id`,`apl_id`),
	KEY `FK_COMPUTER_SOFTWARE`(`apl_id`),
	KEY `FK_SOFTWARE_COMPUTER`(`soft_id`),
	KEY `FK_COMPUTER_SOFT_LICENSE`(`lic_id`),
	CONSTRAINT `R_COMPUTER_SOFTWARE`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_COMPUTER_APPLIANCE`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_COMPUTER_LICENSE`
		FOREIGN KEY (`lic_id`)
		REFERENCES `license` (`lic_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- software_dependency
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `software_dependency`;


CREATE TABLE `software_dependency`
(
	`soft_id` INTEGER  NOT NULL,
	PRIMARY KEY (`soft_id`),
	CONSTRAINT `R_SOFTWARE_DEPENDENCY`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE CASCADE
);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
