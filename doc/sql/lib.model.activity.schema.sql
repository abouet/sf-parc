
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- activity
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `activity`;


CREATE TABLE `activity`
(
	`activity_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`task_id` INTEGER COMMENT 'id of the related task',
	`user_id` INTEGER  NOT NULL COMMENT 'user the entering',
	`type_id` INTEGER  NOT NULL COMMENT 'Activity type',
	`date_start` DATETIME COMMENT 'Activity start',
	`date_end` DATETIME COMMENT 'Activity end',
	`total_day` FLOAT COMMENT 'Total (in days)',
	`all_day` TINYINT COMMENT 'Is the activity all day long',
	`description` VARCHAR(512),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`activity_id`),
	KEY `fk_activity_task`(`task_id`),
	KEY `fk_activity_user`(`user_id`),
	CONSTRAINT `R_ACTIVITY_TASK`
		FOREIGN KEY (`task_id`)
		REFERENCES `task` (`task_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_ACTIVITY_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE CASCADE,
	INDEX `FI_CTIVITY_TYPE` (`type_id`),
	CONSTRAINT `R_ACTIVITY_TYPE`
		FOREIGN KEY (`type_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- info_activitytype
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_activitytype`;


CREATE TABLE `info_activitytype`
(
	`type_id` INTEGER  NOT NULL,
	`type_project` TINYINT COMMENT 'activity on a specific project or internal',
	`type_technical` TINYINT COMMENT 'technical activity',
	`type_charge` TINYINT COMMENT 'wether the activity has to be invoiced',
	`type_code` VARCHAR(5),
	`type_grp` VARCHAR(5),
	PRIMARY KEY (`type_id`),
	KEY `fk_activity_type_info`(`type_id`),
	CONSTRAINT `R_ACTIVITY_TYPE_INFO`
		FOREIGN KEY (`type_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- task
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `task`;


CREATE TABLE `task`
(
	`task_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`assignee_id` INTEGER COMMENT 'user the task is assigned to',
	`ticket_id` INTEGER,
	`task_desc` VARCHAR(100) COMMENT 'description',
	`task_note` TEXT COMMENT 'additonnel information to the task, further indications to the assignee',
	`task_duration` INTEGER COMMENT 'estimated time of ticket completion (in seconds)',
	`task_remain` INTEGER COMMENT 'Remaining estimated percentage',
	`task_recurrence` VARCHAR(100) COMMENT 'Recurring task (cron format)',
	`task_desireddate` DATETIME COMMENT 'date at which the task has to be compeleted',
	`task_duedate` DATETIME COMMENT 'deadline',
	`task_closedate` DATETIME COMMENT 'date at which the task was completed',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`task_id`),
	KEY `FK_TASK_TICKET`(`ticket_id`),
	KEY `FK_TASK_USER`(`assignee_id`),
	KEY `IDX_TASKS`(`assignee_id`, `ticket_id`),
	CONSTRAINT `R_TASK_USER`
		FOREIGN KEY (`assignee_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TASK_TICKET`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- team
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `team`;


CREATE TABLE `team`
(
	`team_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`team_name` VARCHAR(30),
	`leader_id` INTEGER,
	`validator_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`team_id`),
	INDEX `FI_EAM_LEADER` (`leader_id`),
	CONSTRAINT `R_TEAM_LEADER`
		FOREIGN KEY (`leader_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT,
	INDEX `FI_EAM_VALIDATOR` (`validator_id`),
	CONSTRAINT `R_TEAM_VALIDATOR`
		FOREIGN KEY (`validator_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- team_user
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `team_user`;


CREATE TABLE `team_user`
(
	`team_id` INTEGER  NOT NULL,
	`user_id` INTEGER  NOT NULL,
	PRIMARY KEY (`team_id`,`user_id`),
	KEY `FK_USER_TEAM`(`team_id`),
	KEY `FK_TEAM_USER`(`user_id`),
	CONSTRAINT `R_USER_TEAM`
		FOREIGN KEY (`team_id`)
		REFERENCES `team` (`team_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_TEAM_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
