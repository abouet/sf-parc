
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- ticket
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket`;


CREATE TABLE `ticket`
(
	`ticket_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`ticket_ref` INTEGER COMMENT 'External id, used as reférence in external source(email, web...)',
	`ticket_source` CHAR(1) COMMENT 'Source of the ticket (Activity, Email, Phone, Web)',
	`type_id` INTEGER COMMENT 'Type of ticket',
	`submitter_id` INTEGER,
	`ticket_reporter` VARCHAR(32) COMMENT 'Ticket reporter\'s name',
	`ticket_email` VARCHAR(125) COMMENT 'Ticket reporter\'s email',
	`ticket_subject` VARCHAR(64) COMMENT 'Ticket subject',
	`unit_id` INTEGER,
	`apl_id` INTEGER COMMENT 'Department id',
	`soft_id` INTEGER COMMENT 'Software or project id',
	`app_id` INTEGER COMMENT 'Application id',
	`module_id` INTEGER COMMENT 'Module id',
	`version_id` INTEGER COMMENT 'Software version',
	`status_id` INTEGER COMMENT 'Status id',
	`priority_id` INTEGER COMMENT 'Priority id',
	`severity_id` INTEGER COMMENT 'Severity id',
	`ticket_startdate` DATETIME COMMENT 'date the was was taken in charge',
	`ticket_desireddate` DATETIME COMMENT 'date at which the submitter wishes his ticket to be compeleted',
	`ticket_closuredate` DATETIME COMMENT 'date at which the ticket was closed',
	`ticket_duedate` DATETIME COMMENT 'deadline',
	`resolution_id` INTEGER COMMENT 'Reason for why a ticket was closed',
	`ticket_version_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`ticket_id`),
	KEY `FK_TICKET_USER`(`submitter_id`),
	KEY `FK_TICKET_UNIT`(`unit_id`),
	KEY `FK_TICKET_APPLIANCE`(`apl_id`),
	KEY `FK_TICKET_SOFTWARE`(`soft_id`, `version_id`),
	KEY `FK_TICKET_RESOLUTION_VERSION`(`soft_id`, `ticket_version_id`),
	KEY `FK_TICKET_APP`(`app_id`),
	KEY `FK_TICKET_MODULE`(`module_id`),
	KEY `FK_TICKET_STATUS`(`status_id`),
	KEY `FK_TICKET_PRIORITY`(`priority_id`),
	KEY `FK_TICKET_SEVERITY`(`severity_id`),
	KEY `FK_TICKET_RESOLUTION`(`resolution_id`),
	KEY `FK_TICKET_TYPE`(`type_id`),
	KEY `ID_TICKET_REF`(`ticket_ref`),
	CONSTRAINT `R_TICKET_TYPE`
		FOREIGN KEY (`type_id`)
		REFERENCES `info_tickettype` (`type_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_USER`
		FOREIGN KEY (`submitter_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_UNIT`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_APPLIANCE`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_PROJECT`
		FOREIGN KEY (`soft_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_APP`
		FOREIGN KEY (`app_id`)
		REFERENCES `topic` (`topic_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_MODULE`
		FOREIGN KEY (`module_id`)
		REFERENCES `topic` (`topic_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_VERSION`
		FOREIGN KEY (`soft_id`,`version_id`)
		REFERENCES `info_version` (`soft_id`,`version_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_STATUS`
		FOREIGN KEY (`status_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_PRIORITY`
		FOREIGN KEY (`priority_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_SEVERITY`
		FOREIGN KEY (`severity_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_RESOLUTION`
		FOREIGN KEY (`resolution_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_TICKET_RESOLUTION_VERSION`
		FOREIGN KEY (`soft_id`,`ticket_version_id`)
		REFERENCES `info_version` (`soft_id`,`version_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- ticket_message
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_message`;


CREATE TABLE `ticket_message`
(
	`msg_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`parent_id` INTEGER COMMENT 'related message id (in case of response)',
	`ticket_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`msg_type` CHAR(1) COMMENT 'Ticket Message (Description, Additional info, Response, Note ...)',
	`status_id` INTEGER COMMENT 'Status id',
	`author_id` INTEGER  NOT NULL COMMENT 'Author',
	`msg_source` CHAR(1) COMMENT 'Source of the message (Web, Email ...) ',
	`msg_body` TEXT COMMENT 'message',
	`email_header` TEXT COMMENT 'email header (in case of email message)',
	`email_id` VARCHAR(255),
	`ip_address` VARCHAR(16) COMMENT 'IP address',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`msg_id`),
	KEY `FK_MESSAGE_PARENT`(`parent_id`),
	KEY `FK_MESSAGE_TICKET`(`ticket_id`),
	KEY `FK_MESSAGE_USER`(`author_id`),
	KEY `FK_MESSAGE_STATUS`(`status_id`),
	KEY `ID_EMAIL_ID`(`email_id`),
	CONSTRAINT `R_MESSAGE_PARENT`
		FOREIGN KEY (`parent_id`)
		REFERENCES `ticket_message` (`msg_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_MESSAGE_TICKET`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_MESSAGE_STATUS`
		FOREIGN KEY (`status_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_MESSAGE_USER`
		FOREIGN KEY (`author_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- ticket_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_link`;


CREATE TABLE `ticket_link`
(
	`ticket_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`link_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`link_type` CHAR(1) default '0' COMMENT 'Type of the link',
	PRIMARY KEY (`ticket_id`,`link_id`),
	KEY `ID_TICKET_LINK`(`link_id`),
	CONSTRAINT `R_TICKET_LINK`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_LINK_TICKET`
		FOREIGN KEY (`link_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- ticket_history
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_history`;


CREATE TABLE `ticket_history`
(
	`history_id` INTEGER  NOT NULL AUTO_INCREMENT COMMENT 'id of history',
	`ticket_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`user_id` INTEGER  NOT NULL,
	`history_field` VARCHAR(50)  NOT NULL,
	`history_orig` VARCHAR(50),
	`history_value` VARCHAR(50),
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`history_id`),
	KEY `FK_TICKET_HISTORY`(`ticket_id`),
	KEY `FK_USER_TICKET_HISTORY`(`user_id`),
	CONSTRAINT `R_TICKET_HISTORY`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_USER_HISTORY`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- ticket_field
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_field`;


CREATE TABLE `ticket_field`
(
	`ticket_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`field_id` INTEGER  NOT NULL,
	`field_value` VARCHAR(250)  NOT NULL,
	PRIMARY KEY (`ticket_id`,`field_id`),
	KEY `FK_TICKET_FIELD`(`ticket_id`),
	KEY `FK_FIELD_TICKET`(`field_id`),
	CONSTRAINT `R_TICKET_FIELD`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_FIELD_TICKET`
		FOREIGN KEY (`field_id`)
		REFERENCES `project_field` (`field_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- ticket_monitor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_monitor`;


CREATE TABLE `ticket_monitor`
(
	`ticket_id` INTEGER  NOT NULL COMMENT 'id of the related ticket',
	`user_id` INTEGER  NOT NULL,
	PRIMARY KEY (`ticket_id`,`user_id`),
	KEY `FK_MONITORED_TICKET`(`ticket_id`),
	KEY `FK_MONITOR_TICKET`(`user_id`),
	CONSTRAINT `R_MONITORED_TICKET`
		FOREIGN KEY (`ticket_id`)
		REFERENCES `ticket` (`ticket_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_MONITOR_TICKET`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
) COMMENT='monitor ticket, indicates that an email should be sent for each modification';

#-----------------------------------------------------------------------------
#-- info_tickettype
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_tickettype`;


CREATE TABLE `info_tickettype`
(
	`type_id` INTEGER  NOT NULL,
	`type_project` TINYINT COMMENT 'Ticket on a specific project',
	`type_technical` TINYINT COMMENT 'technical Ticket',
	`type_context` TINYINT COMMENT 'has contenxt block',
	`type_date` TINYINT COMMENT 'has dates block',
	`type_properties` TINYINT COMMENT 'has property block',
	`type_info` TINYINT COMMENT 'has additionnal info tab',
	`type_file` TINYINT COMMENT 'has file tab',
	`type_public` TINYINT COMMENT 'The ticket can be created by a user',
	PRIMARY KEY (`type_id`),
	KEY `fk_ticket_type_info`(`type_id`),
	CONSTRAINT `R_TICKET_TYPE_INFO`
		FOREIGN KEY (`type_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- email
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `email`;


CREATE TABLE `email`
(
	`email_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`email_response` TINYINT default 0 COMMENT 'Is auto response enabled',
	`type_id` CHAR(1) COMMENT 'type id',
	`unit_id` INTEGER,
	`proj_id` INTEGER COMMENT 'Project id',
	`app_id` INTEGER COMMENT 'Application id',
	`module_id` INTEGER COMMENT 'Module id',
	`status_id` INTEGER COMMENT 'Status id',
	`priority_id` INTEGER COMMENT 'Priority id',
	`severity_id` INTEGER COMMENT 'Severity id',
	`email_account` VARCHAR(125) COMMENT 'Ticket submitter\'s email',
	`email_name` VARCHAR(30) COMMENT 'Email account name',
	`email_user` VARCHAR(125) COMMENT 'Connection User to server',
	`email_passwd` VARCHAR(125) COMMENT 'Connection password to server',
	`email_active` TINYINT default 1 COMMENT 'Is email account active',
	`srv_id` INTEGER COMMENT 'Mail server',
	`email_fetchfreq` TINYINT COMMENT 'Delay intervals in minutes',
	`email_fetchmax` TINYINT COMMENT 'Maximum emails to process per fetch',
	`email_delete` TINYINT default 0 COMMENT 'Delete fetched messages',
	`email_errors` TINYINT COMMENT 'Number of errors',
	`lasterror_at` DATETIME COMMENT 'date of last error',
	`fetched_at` DATETIME COMMENT 'date last fetched',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`email_id`),
	KEY `FK_EMAIL_SERVER`(`srv_id`),
	INDEX `FI_ICKET_UNIT` (`unit_id`),
	CONSTRAINT `R_TICKET_UNIT`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE RESTRICT,
	INDEX `FI_ICKET_PROJECT` (`proj_id`),
	CONSTRAINT `R_TICKET_PROJECT`
		FOREIGN KEY (`proj_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE RESTRICT,
	INDEX `FI_ICKET_APP` (`app_id`),
	CONSTRAINT `R_TICKET_APP`
		FOREIGN KEY (`app_id`)
		REFERENCES `topic` (`topic_id`)
		ON DELETE RESTRICT,
	INDEX `FI_ICKET_MODULE` (`module_id`),
	CONSTRAINT `R_TICKET_MODULE`
		FOREIGN KEY (`module_id`)
		REFERENCES `topic` (`topic_id`)
		ON DELETE RESTRICT,
	INDEX `FI_ICKET_STATUS` (`status_id`),
	CONSTRAINT `R_TICKET_STATUS`
		FOREIGN KEY (`status_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE SET NULL,
	INDEX `FI_ICKET_PRIORITY` (`priority_id`),
	CONSTRAINT `R_TICKET_PRIORITY`
		FOREIGN KEY (`priority_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE SET NULL,
	INDEX `FI_ICKET_SEVERITY` (`severity_id`),
	CONSTRAINT `R_TICKET_SEVERITY`
		FOREIGN KEY (`severity_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_EMAIL_SERVER`
		FOREIGN KEY (`srv_id`)
		REFERENCES `sync_server` (`srv_id`)
		ON DELETE SET NULL
) COMMENT='Email accounts to synchronize with Ticket';

#-----------------------------------------------------------------------------
#-- info_project
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_project`;


CREATE TABLE `info_project`
(
	`proj_id` INTEGER  NOT NULL,
	`status_id` INTEGER COMMENT 'Status id',
	`email_id` INTEGER COMMENT 'Id of the email address',
	`proj_ispublic` TINYINT,
	PRIMARY KEY (`proj_id`),
	KEY `FK_PROJECT_SOFTWARE`(`proj_id`),
	KEY `FK_PROJECT_STATUS`(`status_id`),
	KEY `ID_PROJ_PUBLIC`(`proj_ispublic`),
	CONSTRAINT `R_PROJECT_SOFTWARE`
		FOREIGN KEY (`proj_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_PROJECT_STATUS`
		FOREIGN KEY (`status_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	INDEX `FI_ROJECT_EMAIL` (`email_id`),
	CONSTRAINT `R_PROJECT_EMAIL`
		FOREIGN KEY (`email_id`)
		REFERENCES `email` (`email_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- project_field
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_field`;


CREATE TABLE `project_field`
(
	`field_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`proj_id` INTEGER  NOT NULL,
	`field_name` VARCHAR(50),
	`field_label` VARCHAR(50),
	`field_help` VARCHAR(50),
	`field_order` SMALLINT,
	`group_id` INTEGER,
	`field_widget` CHAR(1)  NOT NULL COMMENT 'Field widget',
	`field_default` VARCHAR(50),
	`field_values` TEXT,
	`field_display_new` TINYINT default 1 NOT NULL,
	`field_require_new` TINYINT default 1 NOT NULL,
	`field_display_update` TINYINT default 1 NOT NULL,
	`field_require_update` TINYINT default 1 NOT NULL,
	`field_display_resolve` TINYINT default 1 NOT NULL,
	`field_require_resolve` TINYINT default 1 NOT NULL,
	`field_display_close` TINYINT default 1 NOT NULL,
	`field_require_close` TINYINT default 1 NOT NULL,
	`field_min` SMALLINT,
	`field_max` SMALLINT,
	PRIMARY KEY (`field_id`),
	INDEX `FI_ROJECT_FIELD` (`proj_id`),
	CONSTRAINT `R_PROJECT_FIELD`
		FOREIGN KEY (`proj_id`)
		REFERENCES `info_project` (`proj_id`)
		ON DELETE RESTRICT,
	INDEX `FI_IELD_GROUP` (`group_id`),
	CONSTRAINT `R_FIELD_GROUP`
		FOREIGN KEY (`group_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- project_user
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_user`;


CREATE TABLE `project_user`
(
	`projuser_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`proj_id` INTEGER,
	`user_id` INTEGER COMMENT 'User id',
	`team_id` INTEGER COMMENT 'User group',
	PRIMARY KEY (`projuser_id`),
	KEY `FK_PROJECT_USER`(`user_id`),
	KEY `FK_USER_PROJECT`(`proj_id`),
	KEY `FK_PROJECT_TEAM`(`team_id`),
	CONSTRAINT `R_PROJECT_USER`
		FOREIGN KEY (`proj_id`)
		REFERENCES `info_project` (`proj_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_USER_PROJECT`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_PROJECT_TEAM`
		FOREIGN KEY (`team_id`)
		REFERENCES `team` (`team_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- project_history
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_history`;


CREATE TABLE `project_history`
(
	`history_id` INTEGER  NOT NULL AUTO_INCREMENT COMMENT 'id of history',
	`proj_id` INTEGER  NOT NULL COMMENT 'id of the related project',
	`user_id` INTEGER  NOT NULL,
	`history_section` CHAR(1)  NOT NULL COMMENT 'Update section (task, Assignement, Discussion, Comment, File)',
	`history_action` CHAR(1) COMMENT 'Action made on the projet or relations (Creation, update, deletion)',
	`history_value` INTEGER,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`history_id`),
	KEY `FK_PROJECT_HISTORY`(`proj_id`),
	KEY `FK_USER_PROJECT_HISTORY`(`user_id`),
	CONSTRAINT `R_PROJ_FILE_USER`
		FOREIGN KEY (`proj_id`)
		REFERENCES `info_project` (`proj_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_USER_HISTORY`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- file
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `file`;


CREATE TABLE `file`
(
	`file_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`parent_type` CHAR(1) COMMENT 'Type of parent',
	`parent_id` INTEGER  NOT NULL COMMENT 'id of the related parent',
	`file_name` VARCHAR(255) COMMENT 'File name',
	`file_mime` VARCHAR(255) COMMENT 'Mime type',
	`file_size` INTEGER COMMENT 'File size',
	`file_content` BLOB COMMENT 'File content',
	`file__note` TEXT COMMENT 'message for the attachment',
	`created_at` DATETIME,
	`user_id` INTEGER,
	PRIMARY KEY (`file_id`),
	KEY `FK_FILE_USER`(`user_id`),
	CONSTRAINT `R_FILE_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
);

#-----------------------------------------------------------------------------
#-- topic
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `topic`;


CREATE TABLE `topic`
(
	`topic_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`parent_id` INTEGER  NOT NULL,
	`topic_level` SMALLINT  NOT NULL COMMENT 'Project topic level',
	`topic_name` VARCHAR(50)  NOT NULL COMMENT 'Module name',
	`topic_note` TEXT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`topic_id`),
	KEY `FK_TOPIC_PARENT`(`parent_id`),
	CONSTRAINT `R_TOPIC_PARENT`
		FOREIGN KEY (`parent_id`)
		REFERENCES `topic` (`topic_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- project_unit
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `project_unit`;


CREATE TABLE `project_unit`
(
	`unit_id` INTEGER  NOT NULL,
	`proj_id` INTEGER  NOT NULL,
	PRIMARY KEY (`unit_id`,`proj_id`),
	KEY `FK_PROJECT_UNIT`(`unit_id`),
	KEY `FK_UNIT_PROJECT`(`proj_id`),
	CONSTRAINT `R_PROJECT_UNIT`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_UNIT_PROJECT`
		FOREIGN KEY (`proj_id`)
		REFERENCES `info_project` (`proj_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- news
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `news`;


CREATE TABLE `news`
(
	`news_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`proj_id` INTEGER  NOT NULL,
	`parent_id` INTEGER COMMENT ' Comment a discussion',
	`author_id` INTEGER COMMENT 'News author',
	`news_headline` VARCHAR(50) COMMENT 'News headline',
	`news_type` CHAR(1) COMMENT 'Type of news',
	`news_level` CHAR(1) default 'I' COMMENT 'News level (Information, Warning, Alert)',
	`news_body` TEXT COMMENT 'News message',
	`news_displaydate` DATE,
	`news_enddate` DATE,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`news_id`),
	KEY `FK_PROJECT_NEWS`(`proj_id`),
	KEY `FK_PARENT_NEWS`(`parent_id`),
	KEY `FK_USER_NEWS`(`author_id`),
	KEY `IDX_NEWS_LEVEL`(`news_level`),
	CONSTRAINT `R_PROJECT_NEWS`
		FOREIGN KEY (`proj_id`)
		REFERENCES `info_project` (`proj_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_PARENT_NEWS`
		FOREIGN KEY (`parent_id`)
		REFERENCES `news` (`news_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_USER_NEWS`
		FOREIGN KEY (`author_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE RESTRICT
) COMMENT='Global or Project News';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
