
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- apl_type
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `apl_type`;


CREATE TABLE `apl_type`
(
	`type_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`apl_type` CHAR(1) COMMENT 'Appliance class (computer, server, printer, router, phone ...)',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`type_id`),
	UNIQUE KEY `IDX_APL_TYPE` (`type_id`, `apl_type`)
);

#-----------------------------------------------------------------------------
#-- apl_type_label
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `apl_type_label`;


CREATE TABLE `apl_type_label`
(
	`type_id` INTEGER  NOT NULL,
	`lang_id` VARCHAR(2)  NOT NULL,
	`type_label` VARCHAR(50)  NOT NULL,
	`type_cat` VARCHAR(50) COMMENT 'DropdownList regroupment',
	`type_comment` TEXT,
	`type_trans` TINYINT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`type_id`,`lang_id`),
	CONSTRAINT `R_LABEL_APPLIANCESTYPE`
		FOREIGN KEY (`type_id`)
		REFERENCES `apl_type` (`type_id`)
		ON DELETE CASCADE
);

#-----------------------------------------------------------------------------
#-- apl_model
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `apl_model`;


CREATE TABLE `apl_model`
(
	`mftr_id` INTEGER  NOT NULL COMMENT 'Appliance manufacturer',
	`apl_type` CHAR(1)  NOT NULL COMMENT 'Appliance class (computer, server, printer, router, phone ...)',
	`type_id` INTEGER  NOT NULL COMMENT 'Appliance Type (computer, server, printer, router, phone ...)',
	`mdl_id` INTEGER  NOT NULL AUTO_INCREMENT COMMENT 'Model',
	`mdl_name` VARCHAR(255)  NOT NULL COMMENT 'Model name',
	`mdl_img` LONGTEXT COMMENT 'model Photo',
	PRIMARY KEY (`mdl_id`),
	UNIQUE KEY `IDX_APL_MODEL` (`mftr_id`, `apl_type`, `mdl_id`),
	CONSTRAINT `R_MODEL_MANUFACTURER`
		FOREIGN KEY (`mftr_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE CASCADE,
	INDEX `apl_model_FI_2` (`type_id`),
	CONSTRAINT `apl_model_FK_2`
		FOREIGN KEY (`type_id`)
		REFERENCES `apl_type` (`type_id`)
		ON DELETE CASCADE
) COMMENT='Appliance Model';

#-----------------------------------------------------------------------------
#-- apl_group
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `apl_group`;


CREATE TABLE `apl_group`
(
	`group_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`group_name` VARCHAR(50) COMMENT 'Group name',
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`group_id`)
) COMMENT='Appliances group';

#-----------------------------------------------------------------------------
#-- appliance
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `appliance`;


CREATE TABLE `appliance`
(
	`apl_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`apl_type` CHAR(1) COMMENT 'Appliance class (computer, server, printer, router, phone ...)',
	`apl_name` VARCHAR(50) COMMENT 'Appliance name',
	`apl_desc` VARCHAR(256) COMMENT 'Appliance description',
	`apl_sn` VARCHAR(50) COMMENT 'Serial Number',
	`apl_invkey` VARCHAR(50) COMMENT 'Inventory Referevce',
	`apl_comments` TEXT,
	`status_id` INTEGER COMMENT 'State of the hardarware (In service, out of order, out of date...)',
	`unit_id` INTEGER COMMENT 'Unit Id',
	`location_id` INTEGER COMMENT 'Foreign key',
	`type_id` INTEGER COMMENT 'Appliance type',
	`group_id` INTEGER COMMENT 'Group',
	`mftr_id` INTEGER COMMENT 'Appliance manufacturer',
	`mdl_id` INTEGER COMMENT 'Model',
	`tech_id` INTEGER COMMENT 'Technician in charge',
	`apl_dt_purchase` DATE COMMENT 'date of purchase',
	`apl_dt_install` DATE COMMENT 'date of first installation',
	`created_at` DATETIME,
	`updated_at` DATETIME COMMENT 'Modification date',
	PRIMARY KEY (`apl_id`),
	UNIQUE KEY `ID_INVENTORY` (`apl_invkey`),
	KEY `FK_APPLIANCE_TYPE`(`type_id`, `apl_type`),
	KEY `FK_APPLIANCE_GROUP`(`group_id`),
	KEY `FK_APPLIANCE_MODEL`(`mftr_id`, `apl_type`, `mdl_id`),
	KEY `FK_APPLIANCE_MANUFACTURER`(`mftr_id`),
	KEY `FK_APPLIANCE_TECH`(`tech_id`),
	KEY `FK_APPLIANCE_LOCATION`(`unit_id`, `location_id`),
	KEY `ID_APPLIANCE_SN`(`apl_sn`),
	INDEX `FI_PPLIANCE_STATUS` (`status_id`),
	CONSTRAINT `R_APPLIANCE_STATUS`
		FOREIGN KEY (`status_id`)
		REFERENCES `property` (`prop_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_APPL_UNIT`
		FOREIGN KEY (`unit_id`)
		REFERENCES `org_unit` (`unit_id`)
		ON DELETE RESTRICT,
	INDEX `FI_PPL_LOCATION` (`location_id`),
	CONSTRAINT `R_APPL_LOCATION`
		FOREIGN KEY (`location_id`)
		REFERENCES `location` (`location_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_APPLIANCE_TYPE`
		FOREIGN KEY (`type_id`,`apl_type`)
		REFERENCES `apl_type` (`type_id`,`apl_type`)
		ON DELETE SET NULL,
	CONSTRAINT `R_APPLIANCE_GROUP`
		FOREIGN KEY (`group_id`)
		REFERENCES `apl_group` (`group_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_APPLIANCE_MANUFACTURER`
		FOREIGN KEY (`mftr_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE RESTRICT,
	CONSTRAINT `R_APPLIANCE_MODEL`
		FOREIGN KEY (`mftr_id`,`apl_type`,`mdl_id`)
		REFERENCES `apl_model` (`mftr_id`,`apl_type`,`mdl_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_APPLIANCE_TECH`
		FOREIGN KEY (`tech_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE SET NULL
) COMMENT='Appliance of any type (computer, server, printer, router...)';

#-----------------------------------------------------------------------------
#-- info_computer
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_computer`;


CREATE TABLE `info_computer`
(
	`apl_id` INTEGER  NOT NULL,
	`user_id` INTEGER COMMENT 'Computer user',
	`os_id` INTEGER COMMENT 'installed OS',
	`os_version` INTEGER COMMENT 'installed OS version',
	`os_sp` INTEGER COMMENT 'installed OS Service Pack',
	`cmp_domain` VARCHAR(50) COMMENT 'Network domain or Workgroup',
	`cmp_auto_update` TINYINT default 1 COMMENT 'To be updated automatically',
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_COMPUTER`(`apl_id`),
	KEY `FK_COMPUTER_USER`(`user_id`),
	KEY `IDX_OS_COMPUTER`(`os_id`, `os_version`, `os_sp`),
	CONSTRAINT `R_COMPUTER_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_COMPUTER_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE SET NULL,
	CONSTRAINT `R_COMPUTER_OS`
		FOREIGN KEY (`os_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL,
	INDEX `FI_OMPUTER_VERSION` (`os_version`),
	CONSTRAINT `R_COMPUTER_VERSION`
		FOREIGN KEY (`os_version`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL,
	INDEX `FI_OMPUTER_SP` (`os_sp`),
	CONSTRAINT `R_COMPUTER_SP`
		FOREIGN KEY (`os_sp`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL
) COMMENT='additional characteristics for computers';

#-----------------------------------------------------------------------------
#-- info_server
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_server`;


CREATE TABLE `info_server`
(
	`apl_id` INTEGER  NOT NULL,
	`os_id` INTEGER COMMENT 'installed OS',
	`os_version` INTEGER COMMENT 'installed OS version',
	`os_sp` INTEGER COMMENT 'installed OS Service Pack',
	`srv_domain` VARCHAR(50) COMMENT 'Network domain or Workgroup',
	`srv_ip` VARCHAR(16) COMMENT 'server IP Address',
	`srv_auto_update` TINYINT default 1 COMMENT 'To be updated automatically',
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_SERVER`(`apl_id`),
	KEY `IDX_OS_SERVER`(`os_id`, `os_version`, `os_sp`),
	CONSTRAINT `R_SERVER_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_SERVER_OS`
		FOREIGN KEY (`os_id`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL,
	INDEX `FI_ERVER_VERSION` (`os_version`),
	CONSTRAINT `R_SERVER_VERSION`
		FOREIGN KEY (`os_version`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL,
	INDEX `FI_ERVER_SP` (`os_sp`),
	CONSTRAINT `R_SERVER_SP`
		FOREIGN KEY (`os_sp`)
		REFERENCES `software` (`soft_id`)
		ON DELETE SET NULL
) COMMENT='additional characteristics for servers';

#-----------------------------------------------------------------------------
#-- info_printer
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_printer`;


CREATE TABLE `info_printer`
(
	`apl_id` INTEGER  NOT NULL,
	`prt_port` VARCHAR(255) COMMENT 'Port name',
	`prt_serial` CHAR(1) COMMENT 'indicator wether printer has serial port or not',
	`prt_par` CHAR(1) COMMENT 'indicator wether printer has parallel port or not',
	`ptr_usb` CHAR(1) COMMENT 'indicator wether printer has USB port or not',
	`prt_ram` VARCHAR(255) COMMENT 'RAM size',
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_PRINTER`(`apl_id`),
	CONSTRAINT `R_PRINTER_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE
) COMMENT='additional characteristics for printers';

#-----------------------------------------------------------------------------
#-- info_monitor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_monitor`;


CREATE TABLE `info_monitor`
(
	`apl_id` INTEGER  NOT NULL,
	`mtr_size` SMALLINT COMMENT 'Screen size',
	`mtr_micro` CHAR(1),
	`mtr_speaker` CHAR(1),
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_MONITOR`(`apl_id`),
	CONSTRAINT `R_MONITOR_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE
) COMMENT='additional characteristics for monitors';

#-----------------------------------------------------------------------------
#-- info_phone
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_phone`;


CREATE TABLE `info_phone`
(
	`apl_id` INTEGER  NOT NULL,
	`phn_number` VARCHAR(15) COMMENT 'Phone Number',
	`phn_ext` VARCHAR(15) COMMENT 'internal line extension',
	`user_id` INTEGER COMMENT 'user',
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_PHONE`(`apl_id`),
	KEY `FK_PHONE_USER`(`user_id`),
	CONSTRAINT `R_PHONE_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE,
	CONSTRAINT `R_PHONE_USER`
		FOREIGN KEY (`user_id`)
		REFERENCES `info_user` (`user_id`)
		ON DELETE SET NULL
) COMMENT='additional characteristics for phones';

#-----------------------------------------------------------------------------
#-- info_router
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `info_router`;


CREATE TABLE `info_router`
(
	`apl_id` INTEGER  NOT NULL,
	`rtr_ip` VARCHAR(16) COMMENT 'router IP Address',
	`rtr_mac` VARCHAR(255) COMMENT 'router MAC Address',
	PRIMARY KEY (`apl_id`),
	KEY `FK_APPLIANCE_ROUTER`(`apl_id`),
	CONSTRAINT `R_ROUTER_APL`
		FOREIGN KEY (`apl_id`)
		REFERENCES `appliance` (`apl_id`)
		ON DELETE CASCADE
) COMMENT='additional characteristics for routers';

#-----------------------------------------------------------------------------
#-- mftr_apl_type
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `mftr_apl_type`;


CREATE TABLE `mftr_apl_type`
(
	`mftr_id` INTEGER  NOT NULL,
	`apl_type` CHAR(1)  NOT NULL COMMENT 'Appliance class (computer, server, printer, router, phone ...)',
	PRIMARY KEY (`mftr_id`,`apl_type`),
	CONSTRAINT `R_APPLIANCE_MANUFACTURER`
		FOREIGN KEY (`mftr_id`)
		REFERENCES `company` (`cie_id`)
		ON DELETE CASCADE
);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
