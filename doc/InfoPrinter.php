<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPrinter
 *
 * @ORM\Table(name="info_printer", indexes={@ORM\Index(name="FK_APPLIANCE_PRINTER", columns={"apl_id"})})
 * @ORM\Entity
 */
class InfoPrinter
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="prt_port", type="string", length=255, nullable=true, options={"comment"="Port name"})
     */
    private $prtPort;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prt_serial", type="string", length=1, nullable=true, options={"fixed"=true,"comment"="indicator wether printer has serial port or not"})
     */
    private $prtSerial;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prt_par", type="string", length=1, nullable=true, options={"fixed"=true,"comment"="indicator wether printer has parallel port or not"})
     */
    private $prtPar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ptr_usb", type="string", length=1, nullable=true, options={"fixed"=true,"comment"="indicator wether printer has USB port or not"})
     */
    private $ptrUsb;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prt_ram", type="string", length=255, nullable=true, options={"comment"="RAM size"})
     */
    private $prtRam;

    /**
     * @var \Appliance
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Appliance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apl_id", referencedColumnName="apl_id")
     * })
     */
    private $apl;


}
