<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPhone
 *
 * @ORM\Table(name="info_phone", indexes={@ORM\Index(name="FK_PHONE_USER", columns={"user_id"}), @ORM\Index(name="FK_APPLIANCE_PHONE", columns={"apl_id"})})
 * @ORM\Entity
 */
class InfoPhone
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="phn_number", type="string", length=15, nullable=true, options={"comment"="Phone Number"})
     */
    private $phnNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phn_ext", type="string", length=15, nullable=true, options={"comment"="internal line extension"})
     */
    private $phnExt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true, options={"comment"="user"})
     */
    private $userId;

    /**
     * @var \Appliance
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Appliance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apl_id", referencedColumnName="apl_id")
     * })
     */
    private $apl;


}
