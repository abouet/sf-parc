<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1409718936274" ID="ID_293404717" LINK="../../../HelpdeskBundle/Resources/doc/helpdesk.mm" MODIFIED="1409722942947" TEXT="activity">
<node CREATED="1409719484219" MODIFIED="1409722864630" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      toDo list
    </p>
    <p style="text-align: center">
      <font size="2">(by user)</font>
    </p>
  </body>
</html></richcontent>
<node CREATED="1409719812462" MODIFIED="1409719825065" TEXT="timesheet">
<node CREATED="1409719830467" MODIFIED="1409719840847" TEXT="validation"/>
<node CREATED="1409719951996" MODIFIED="1409720629733" TEXT="description">
<node CREATED="1409719935096" MODIFIED="1409719948621" TEXT="type of action">
<node CREATED="1409720159924" MODIFIED="1409720176560" TEXT="details accordingly"/>
</node>
<node CREATED="1409720629723" MODIFIED="1409721204617" TEXT="Time">
<node CREATED="1409719980182" MODIFIED="1409720006299" TEXT="estimated time left"/>
<node CREATED="1409720018444" MODIFIED="1409720028480" TEXT="full-day">
<node CREATED="1409721162863" MODIFIED="1409721180327" TEXT="span over several days"/>
</node>
<node CREATED="1409720033244" MODIFIED="1409721361754">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      total time
    </p>
    <p style="text-align: center">
      <font size="2">(within a day)</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1409720043485" MODIFIED="1409721393869">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      specific time
    </p>
    <p style="text-align: center">
      <font size="2">(start/end time within a day)</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1409721204617" MODIFIED="1409721298385">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      stored second
    </p>
    <p style="text-align: center">
      <font size="2">(for total and format purpose)</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1409719549163" MODIFIED="1409719722388" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      total time
    </p>
    <p style="text-align: center">
      <font size="2">spent on a ticket</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</map>
