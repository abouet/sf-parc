<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379843763716" ID="ID_1160915806" MODIFIED="1409719344058" TEXT="Helpsdesk">
<hook NAME="accessories/plugins/HierarchicalIcons.properties"/>
<node CREATED="1379843830856" ID="ID_29925854" MODIFIED="1409719413804" POSITION="right" TEXT="Project">
<node CREATED="1379843960298" ID="ID_1302739415" MODIFIED="1379855034598" TEXT="Bug"/>
<node CREATED="1379843982538" ID="ID_448643343" MODIFIED="1379855044814" TEXT="Feature"/>
<node CREATED="1380016527999" ID="ID_526312912" MODIFIED="1409719416521" TEXT="toDo list">
<node CREATED="1380016551174" ID="ID_222083915" MODIFIED="1409722824857" TEXT="Attachment">
<node CREATED="1409721455478" ID="ID_1582252128" MODIFIED="1409721471452" TEXT="message"/>
</node>
<node CREATED="1380016623035" ID="ID_1461826200" MODIFIED="1380016646686" TEXT="Forum"/>
<node CREATED="1380016657783" ID="ID_913639920" MODIFIED="1380016669578" TEXT="Tasks">
<node CREATED="1409719447560" ID="ID_1650148892" LINK="#ID_533769928" MODIFIED="1409723179021">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      assign to user
    </p>
    <p style="text-align: center">
      <font size="2">(can be null if not assigned)</font>
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_1369596883" ENDARROW="Default" ENDINCLINATION="620;0;" ID="Arrow_ID_1180281797" STARTARROW="None" STARTINCLINATION="620;0;"/>
</node>
<node CREATED="1411406803271" ID="ID_1061368071" MODIFIED="1411533264623">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      assign to a team
    </p>
    <p style="text-align: center">
      <font size="1">user within the team : </font>
    </p>
    <p>
      <font size="1">&#160;&#160;&#160;&#160;&#160;- can see them </font>
    </p>
    <p>
      <font size="1">&#160;&#160;&#160;&#160;&#160;- take them into account</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1409721438374" ID="ID_1399732389" MODIFIED="1409721447578" TEXT="description"/>
</node>
</node>
<node CREATED="1379854994673" ID="ID_1867128343" MODIFIED="1409719084494" POSITION="left" TEXT="Hotline">
<node CREATED="1379855060343" ID="ID_365782578" MODIFIED="1379855185067" TEXT="Dysfunction">
<node CREATED="1380097388282" ID="ID_1904555899" MODIFIED="1409717976223" TEXT="intervention">
<node CREATED="1380097491896" ID="ID_1094405656" MODIFIED="1380097522313" TEXT="maintenance sheet"/>
<node CREATED="1409718378295" ID="ID_1379066782" MODIFIED="1409719707263">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      export to portable device
    </p>
    <p style="text-align: center">
      <font size="2">(smartphone, tablet)</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1379855088011" ID="ID_1400777839" MODIFIED="1409721697247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      grant access
    </p>
    <p style="text-align: center">
      <font size="2">(to an application, rights, machine...)</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1379855109915" ID="ID_783438363" MODIFIED="1409723052736" POSITION="right" TEXT="Task">
<node CREATED="1379855122967" ID="ID_1580370982" MODIFIED="1379855135257" TEXT="Recurring"/>
<node CREATED="1379863037348" ID="ID_1007451005" MODIFIED="1379863053035" TEXT="Export">
<node CREATED="1379863069756" ID="ID_1582665133" MODIFIED="1379864146466" TEXT="Scrum tasks"/>
<node CREATED="1379864318591" ID="ID_326008767" MODIFIED="1379864328293" TEXT="vcard RCF"/>
</node>
<node CREATED="1409723071108" ID="ID_581854488" LINK="#ID_1369596883" MODIFIED="1409723263993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      assign to user
    </p>
    <p style="text-align: center">
      <font size="2">(can be null if not assigned)</font>
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_1369596883" ENDARROW="Default" ENDINCLINATION="409;0;" ID="Arrow_ID_946766646" STARTARROW="None" STARTINCLINATION="409;0;"/>
</node>
</node>
<node COLOR="#000000" CREATED="1379863143134" ID="ID_1555307358" MODIFIED="1409719059059" POSITION="left" STYLE="fork" TEXT="import">
<node CREATED="1379863153525" ID="ID_616651379" MODIFIED="1409719059059" TEXT="email"/>
<node COLOR="#000000" CREATED="1379863163052" ID="ID_1854725904" MODIFIED="1409719059060">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      other app
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379864120725" ID="ID_1604099156" MODIFIED="1409719059060" TEXT="scrum tasks"/>
<node CREATED="1380097630245" ID="ID_1814418432" MODIFIED="1409719059061" TEXT="Data recovery"/>
</node>
<node CREATED="1409717850731" ID="ID_1440453304" MODIFIED="1409718265066" POSITION="left" TEXT="Export">
<node CREATED="1409718265066" ID="ID_1236368555" MODIFIED="1409718274722" TEXT="scrum task"/>
</node>
<node CREATED="1409717996120" ID="ID_715836201" MODIFIED="1409718341927" POSITION="right" TEXT="Fix">
<node CREATED="1409718039615" ID="ID_602159035" LINK="#ID_783438363" MODIFIED="1409723284886" TEXT="task">
<arrowlink DESTINATION="ID_783438363" ENDARROW="Default" ENDINCLINATION="63;0;" ID="Arrow_ID_51633633" STARTARROW="None" STARTINCLINATION="63;0;"/>
</node>
<node CREATED="1409718049182" ID="ID_83585496" MODIFIED="1409718064961" TEXT="attachements"/>
<node CREATED="1409718076287" ID="ID_676343265" MODIFIED="1409718185482" TEXT="resolution">
<node CREATED="1409718084665" ID="ID_208668897" MODIFIED="1409719750177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      KB
    </p>
    <p style="text-align: center">
      <font size="2">(export analysis &amp; fix in wiki)</font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1409718186487" ID="ID_1049003890" MODIFIED="1409718192686" TEXT="cause"/>
<node CREATED="1409718201977" ID="ID_764231260" MODIFIED="1409718219388" TEXT="liaibility"/>
<node CREATED="1409718222104" ID="ID_1762543995" MODIFIED="1409718233695" TEXT="remedy"/>
</node>
</node>
<node CREATED="1409718838731" ID="ID_1417333303" MODIFIED="1409718900167" POSITION="right" TEXT="news">
<node CREATED="1409718846679" ID="ID_1078862075" MODIFIED="1409718851985" TEXT="public"/>
<node CREATED="1409718853476" ID="ID_347388751" MODIFIED="1409718866075" TEXT="project specific"/>
<node CREATED="1409717872190" ID="ID_1154263214" MODIFIED="1409717875273" TEXT="rss"/>
</node>
<node CREATED="1409722943005" ID="ID_1369596883" LINK="../../../ActivityBundle/Resources/doc/activity.mm" MODIFIED="1409723179020" POSITION="left" TEXT="activity"/>
<node CREATED="1409722285197" ID="ID_654340989" MODIFIED="1409722312487" POSITION="right" TEXT="add specific fields">
<node CREATED="1409722315545" ID="ID_1390300859" MODIFIED="1409722359831" TEXT="specific to a project"/>
<node CREATED="1409722362656" ID="ID_1518878226" LINK="#ID_1395876741" MODIFIED="1409723214135" TEXT="specific to a status">
<arrowlink DESTINATION="ID_1395876741" ENDARROW="Default" ENDINCLINATION="669;0;" ID="Arrow_ID_1301311189" STARTARROW="None" STARTINCLINATION="669;0;"/>
</node>
</node>
<node CREATED="1409722389909" ID="ID_163787114" MODIFIED="1409722574454" POSITION="left" TEXT="status">
<node CREATED="1409722420996" ID="ID_1039636284" MODIFIED="1409722434894" TEXT="restricted">
<node CREATED="1409722437736" ID="ID_422543955" MODIFIED="1409722453194" TEXT="according to rights"/>
<node CREATED="1409722455689" ID="ID_1603204561" MODIFIED="1409722542491" TEXT="in order"/>
</node>
<node CREATED="1409722574455" ID="ID_1395876741" LINK="#ID_1518878226" MODIFIED="1409723214133" TEXT="additionnal information"/>
</node>
</node>
</map>
