<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1410969318532" ID="ID_1861174759" MODIFIED="1410969816336" TEXT="dashboard">
<node CREATED="1410969356328" ID="ID_461897135" MODIFIED="1410969402317" POSITION="right" TEXT="user defined">
<node CREATED="1410969404091" ID="ID_1645220016" MODIFIED="1410969410172" TEXT="position">
<node CREATED="1409835412660" ID="ID_1802159209" MODIFIED="1409835421942" TEXT="drag&amp;drop"/>
</node>
<node CREATED="1410969412520" ID="ID_824062041" MODIFIED="1410969426717" TEXT="data"/>
<node CREATED="1410969428289" ID="ID_1043536465" MODIFIED="1410969431997" TEXT="type">
<node CREATED="1409822814898" ID="ID_639003049" MODIFIED="1409824234965" TEXT="graph">
<node CREATED="1409823230952" ID="ID_1803807904" MODIFIED="1409835549674">
<richcontent TYPE="NODE"><html>
                            <head>

                            </head>
                            <body>
                                <p style="text-align: center">
                                    chart type
                                </p>
                                <p style="text-align: center">
                                    <font size="2">(pie, bar, candle...)</font>
                                </p>
                            </body>
                        </html></richcontent>
<node CREATED="1409823240429" ID="ID_1538223802" MODIFIED="1409824234950" TEXT="combine multiple charts">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1410969590832" ID="ID_1682203501" MODIFIED="1410969599583" TEXT="single figure"/>
</node>
<node CREATED="1409822800944" ID="ID_1707118297" MODIFIED="1409824234965" TEXT="indicator">
<node CREATED="1409822987644" ID="ID_1760943819" MODIFIED="1409824234965">
<richcontent TYPE="NODE"><html>
                            <head>

                            </head>
                            <body>
                                <p style="text-align: center">
                                    single figure &amp; text
                                </p>
                                <p style="text-align: center">
                                    <font size="2">(eg : 3 active tickets)</font>
                                </p>
                            </body>
                        </html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1409831934919" ID="ID_1489248588" MODIFIED="1409833754750">
<richcontent TYPE="NODE"><html>
                            <head>

                            </head>
                            <body style="text-align: center">
                                <p style="text-align: center">
                                    Mise en page HTML
                                </p>
                                <img src="indicator.png" />
                            </body>
                        </html></richcontent>
</node>
</node>
<node CREATED="1409822850728" ID="ID_414388000" MODIFIED="1409824234965" TEXT="tables">
<node CREATED="1409823168568" ID="ID_442503210" MODIFIED="1409824234965" TEXT="field alias = column header"/>
<node CREATED="1409823214845" ID="ID_1403892931" MODIFIED="1409824234965" TEXT="limit"/>
</node>
</node>
</node>
<node CREATED="1410969825518" ID="ID_913192566" MODIFIED="1410969837550" POSITION="left" TEXT="application dependent"/>
</node>
</map>
