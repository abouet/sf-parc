<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1411405401494" ID="ID_322089518" MODIFIED="1432138169180">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      <img src="../images/logo.png" />
      
    </p>
    <p style="text-align: center">
      <font size="5"><b>sfParc</b></font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1409836298254" ID="ID_862919233" MODIFIED="1432138169107" POSITION="right" TEXT="project management">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1409836757144" ID="ID_842947222" MODIFIED="1432314388827">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icons/note_edit.png" />
      &#160;discussions
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      toto
    </p>
  </body>
</html>
</richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836773688" ID="ID_1653768522" MODIFIED="1432314453686" TEXT="toDo list">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1411405873482" ID="ID_136338230" MODIFIED="1432138169108" TEXT="new features">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1411405892007" ID="ID_75375707" MODIFIED="1432138169114">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icons/bug.png" />
      &#160;Bug fixes
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1411405914143" ID="ID_1310100932" MODIFIED="1432138169118" TEXT="evolutions">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1411405962303" ID="ID_1986310840" MODIFIED="1432138169122">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      actions to undertake (tasks)
    </p>
    <p style="text-align: center">
      <font size="1">meeting, doc to write...</font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1409836793773" ID="ID_71961933" MODIFIED="1432138169126" TEXT="attachements">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836829848" ID="ID_467556797" MODIFIED="1432138169126" TEXT="manage version">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1409836848575" ID="ID_263291407" MODIFIED="1432138169126" TEXT="roadmap">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1409836862592" ID="ID_256363392" MODIFIED="1432138169126" TEXT="release note">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1409836910741" ID="ID_1260738424" MODIFIED="1432138169135">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icon-group.png" />
      &#160;Team
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1409836953368" ID="ID_584544241" MODIFIED="1432138169142">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icon-permission.png" />
      &#160;Credentials
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1411406744384" ID="ID_1302471565" MODIFIED="1432138295724">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icon_task.gif" />
      tasks
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1409836405038" ID="ID_502048149" MODIFIED="1432138169162" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icon-ticket.png" />
      &#160;Ticket
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1409836426184" ID="ID_283343502" MODIFIED="1432138924268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icons/error.png" />
      &#160;report an incident
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1409836638648" ID="ID_671734979" MODIFIED="1432138169163" TEXT="on a project">
<arrowlink DESTINATION="ID_75375707" ENDARROW="Default" ENDINCLINATION="695;-85;" ID="Arrow_ID_1037822977" STARTARROW="None" STARTINCLINATION="279;-278;"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1409836656112" ID="ID_1291376066" MODIFIED="1432138658435" TEXT="of an appliance">
<arrowlink DESTINATION="ID_1689474484" ENDARROW="Default" ENDINCLINATION="38;230;" ID="Arrow_ID_1302125637" STARTARROW="None" STARTINCLINATION="321;154;"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1432138781721" ID="ID_1526405289" MODIFIED="1432138781722" TEXT="">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836445177" ID="ID_813885571" MODIFIED="1432138656962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icons/asterisk_orange.png" />
      &#160;manage changes
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836457680" ID="ID_1612983484" MODIFIED="1432138713579">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icons/award_star_bronze_2.png" />
      ask for access
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1409836479060" ID="ID_1618639434" MODIFIED="1432138355594" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="../images/icon-inventory.png" />
      &#160;inventory
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1409836498646" ID="ID_636531712" MODIFIED="1432138169178" TEXT="keep track of appliances">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1411406601089" ID="ID_680778100" MODIFIED="1432138169179" TEXT="trace related contracts">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836531764" ID="ID_1703244719" MODIFIED="1432138169179" TEXT="assign appliances to user">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836551787" ID="ID_990412737" MODIFIED="1432138169179" TEXT="put a technician in charge">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836591091" ID="ID_653866554" MODIFIED="1432138169180" TEXT="manage licences">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1409836679441" ID="ID_1689474484" MODIFIED="1432138658436" TEXT="track incidents">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
