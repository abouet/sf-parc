<!--<map version="1.0.1">-->
<map version="0.9.0">
    <!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
    <node CREATED="1409836271726" ID="ID_1346296213" MODIFIED="1409836288231" TEXT="sfParc">
        <node CREATED="1409836298254" ID="ID_862919233" MODIFIED="1409836328871" POSITION="right" TEXT="project management">
            <node CREATED="1409836757144" ID="ID_842947222" MODIFIED="1409836766801" TEXT="discussions"/>
            <node CREATED="1409836773688" ID="ID_1653768522" MODIFIED="1409836781753" TEXT="toDo list"/>
            <node CREATED="1409836793773" ID="ID_71961933" MODIFIED="1409836801152" TEXT="attachements"/>
            <node CREATED="1409836829848" ID="ID_467556797" MODIFIED="1409836891015" TEXT="manage version">
                <node CREATED="1409836848575" ID="ID_263291407" MODIFIED="1409836853271" TEXT="roadmap"/>
                <node CREATED="1409836862592" ID="ID_256363392" MODIFIED="1409836874729" TEXT="release note"/>
            </node>
            <node CREATED="1409836910741" ID="ID_1260738424" MODIFIED="1409836914501" TEXT="team">
                <node CREATED="1409836953368" ID="ID_584544241" MODIFIED="1409836965050" TEXT="credentials"/>
            </node>
        </node>
        <node CREATED="1409836405038" ID="ID_502048149" MODIFIED="1409836423212" POSITION="left" TEXT="Ticket">
            <node CREATED="1409836426184" ID="ID_283343502" MODIFIED="1409837066808" TEXT="report an incident">
                <node CREATED="1409836638648" ID="ID_671734979" MODIFIED="1409836650129" TEXT="on a project"/>
                <node CREATED="1409836656112" ID="ID_1291376066" MODIFIED="1409837117867" TEXT="of an appliance">
                    <linktarget COLOR="#b0b0b0" DESTINATION="ID_1291376066" ENDARROW="Default" ENDINCLINATION="552;0;" ID="Arrow_ID_1302125637" SOURCE="ID_1689474484" STARTARROW="None" STARTINCLINATION="553;0;"/>
                </node>
            </node>
            <node CREATED="1409836445177" ID="ID_813885571" MODIFIED="1409836455223" TEXT="manage changes"/>
            <node CREATED="1409836457680" ID="ID_1612983484" MODIFIED="1409836470675" TEXT="ask for access"/>
        </node>
        <node CREATED="1409836479060" ID="ID_1618639434" MODIFIED="1409836488607" POSITION="right" TEXT="inventory">
            <node CREATED="1409836498646" ID="ID_636531712" MODIFIED="1409836523918" TEXT="keep track of appliances"/>
            <node CREATED="1409836531764" ID="ID_1703244719" MODIFIED="1409836547801" TEXT="assign appliances to user"/>
            <node CREATED="1409836551787" ID="ID_990412737" MODIFIED="1409836584110" TEXT="put a technician in charge"/>
            <node CREATED="1409836591091" ID="ID_653866554" MODIFIED="1409836616972" TEXT="manage licences"/>
            <node CREATED="1409836679441" ID="ID_1689474484" MODIFIED="1409837117867" TEXT="track incidents">
                <arrowlink DESTINATION="ID_1291376066" ENDARROW="Default" ENDINCLINATION="552;0;" ID="Arrow_ID_1302125637" STARTARROW="None" STARTINCLINATION="553;0;"/>
            </node>
        </node>
    </node>
</map>
