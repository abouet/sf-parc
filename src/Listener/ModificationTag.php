<?php

namespace App\Listener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\Core\BaseEntity;

/**
 * Description of ModificationTagSubscriber
 *
 * @author abouet
 */
class ModificationTag {

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        if (property_exists($entity, BaseEntity::CREATION_DATE)) {
            $entity->setCreationDate(new \DateTime());
        }
        if (property_exists($entity, BaseEntity::CREATION_USER)) {
            //$entity->setCreationUser();
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        if (property_exists($entity, BaseEntity::UPDATE_DATE)) {
            $entity->setUpdateDate(new \DateTime());
        }
        if (property_exists($entity, BaseEntity::UPDATE_USER)) {
            //$entity->setUpdateUser();
        }
    }

}
