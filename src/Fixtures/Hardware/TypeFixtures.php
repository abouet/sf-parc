<?php

namespace App\Fixtures\Hardware;

use App\Entity\Hardware\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TypeFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $type = new Type();
        $type->setLabel('Mini Tower');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Desktop');
        $manager->persist($type);

       $type = new Type();
        $type->setLabel('Compact');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Ultra compact');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Tablet');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Laptop');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Tower');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Rack');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Lame');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Wireless DECT');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Smartphone');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Laser B&W');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Laser B&W Multifunction');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Laser Color');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Laser Multifunction');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Deskjet Color');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Deskjet B&W');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Deskjet Color Multifunction');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Fax');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Virtual');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Thermic');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Standard');
        $manager->persist($type);

        $type->setLabel('Flat');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Rack');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('DSL');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Standard');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('VLAN');
        $manager->persist($type);

        $type = new Type();
        $type->setLabel('Rack');
        $manager->persist($type);

        $manager->flush();
    }

}
