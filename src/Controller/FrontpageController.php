<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontpageController extends AbstractController {

    /**
     * @Route("/", name="frontpage")
     */
    public function index() {
        return $this->render('index.html.twig', [
                    'controller_name' => 'FrontpageController',
        ]);
    }

    /**
     * @Route("/widget", name="widgets")
     */
    public function widgets() {
        return $this->render('widgets.html.twig');
    }

    /**
     * @Route("/tables", name="tables")
     */
    public function tables() {
        return $this->render('tables.html.twig');
    }

    /**
     * @Route("/forms", name="forms")
     */
    public function forms() {
        return $this->render('forms.html.twig');
    }

}
