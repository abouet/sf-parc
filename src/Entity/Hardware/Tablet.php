<?php

namespace App\Entity\Hardware;

/**
 * Description of Tablet
 *
 */
class Tablet extends Computer {

    public function __construct() {
        Parent::__construct();
        $this->isLaptop = false;
        $this->isTablet = true;
    }

}
