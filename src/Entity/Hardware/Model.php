<?php

namespace App\Entity\Hardware;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Core\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\ModelRepository")
 */
class Model extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $class;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hardware\Device", mappedBy="model")
     */
    protected $devices;

    public function __construct()
    {
        $this->devices = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getClass(): ?string {
        return $this->class;
    }

    public function setClass(string $class): self {
        $this->class = $class;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->desc;
    }

    public function setDescription(string $desc): self {
        $this->description = $desc;
        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;
        return $this;
    }

    public function __toString(): string {
        return (string) $this->getName();
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setModel($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getModel() === $this) {
                $device->setModel(null);
            }
        }

        return $this;
    }

}
