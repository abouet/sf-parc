<?php

namespace App\Entity\Hardware;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardawre\PrinterRepository")
 */
class Printer extends Device implements DeviceInterface, InheritanceTableInterface {

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $hasUSB;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isWifi;

    /**
     * RAM size
     * 
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $ram;

    public function hasUSB(): ?bool {
        return $this->hasUSB;
    }

    public function setUSB(bool $hasUSB = true): self {
        $this->hasUSB = $hasUSB;

        return $this;
    }

    public function isWifi(): ?bool {
        return $this->isWifi;
    }

    public function setWifi(bool $isWifi = true): self {
        $this->isWifi = $isWifi;

        return $this;
    }

    public function getRam(): ?int {
        return $this->ram;
    }

    public function setRam(?int $ram): self {
        $this->ram = $ram;

        return $this;
    }

    public function getHasUSB(): ?bool
    {
        return $this->hasUSB;
    }

    public function setHasUSB(bool $hasUSB): self
    {
        $this->hasUSB = $hasUSB;

        return $this;
    }

    public function getIsWifi(): ?bool
    {
        return $this->isWifi;
    }

    public function setIsWifi(bool $isWifi): self
    {
        $this->isWifi = $isWifi;

        return $this;
    }

}
