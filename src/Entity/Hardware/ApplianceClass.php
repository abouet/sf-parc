<?php

namespace App\Entity\Hardware;

/**
 * Description of ApplianceType
 *
 * @author abouet
 */
class ApplianceClass {

    public const CODE_COMPUTER = "C";
    public const LABEL_COMPUTER = "Computer";
    public const CODE_PRINTER = "P";
    public const LABEL_PRINTER = "Printer";
    public const CODE_PHONE = "T";
    public const LABEL_PHONE = "Phone";
    public const CODE_SCREEN = "M";
    public const LABEL_SCREEN = "Screen";
//    router_rack
//    switch_vlan
    public const CODE_SERVER = "S";
    public const LABEL_SERVER = "Server";
    public const TYPES = [
        self::CODE_COMPUTER => self::LABEL_COMPUTER,
        self::CODE_PRINTER => self::LABEL_PRINTER,
        self::CODE_PHONE => self::LABEL_PHONE,
        self::CODE_SCREEN => self::LABEL_SCREEN,
        self::CODE_SERVER => self::LABEL_SERVER,
    ];

    private $code;
    private $label;

    public function __construct($code, $label) {
        $this->code = $code;
        $this->label = $label;
    }

    public function getCode(): string {
        return $this->code;
    }

    public function getLabel(): string {
        return $this->label;
    }

}
