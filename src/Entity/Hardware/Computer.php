<?php

namespace App\Entity\Hardware;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
 use\Doctrine\Common\Collections\ArrayCollection;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\ComputerRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *      "": "Computer",
 *      "Tablet": "Tablet",
 *      "Laptop": "Laptop",
 *      "Server": "Server"
 * })
 */
class Computer extends Device implements DeviceInterface, InheritanceTableInterface {

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $domain;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAutoUpdate;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isLaptop;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isTablet;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isServer;

    /**
     * A computer has one or more monitors.
     * 
     * @ORM\ManyToMany(targetEntity="Screen", inversedBy="computers")
     * @ORM\JoinTable(name="computer_screen")
     */
    protected $monitors;
    protected $os;
    protected $osVersion;
    protected $servicePack;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Software\SoftwareImplementation", mappedBy="computer")
     */
    protected $implementations;

    public function __construct() {
        Parent::__construct();
        $this->licenses = new ArrayCollection();
        $this->implementations = new ArrayCollection();
        $this->monitors = new ArrayCollection();
    }

    public function isAutoUpdate(): ?bool {
        return $this->isAutoUpdate;
    }

    public function setAutoUpdate(bool $bool = true): self {
        $this->isAutoUpdate = $bool;

        return $this;
    }

    /**
     * @return Collection|Screen[]
     */
    public function getMonitors(): Collection {
        return $this->monitors;
    }

    public function addMonitor(Screen $monitor): self {
        if (!$this->monitors->contains($monitor)) {
            $this->monitors[] = $monitor;
        }

        return $this;
    }

    public function removeMonitor(Screen $monitor): self {
        if ($this->monitors->contains($monitor)) {
            $this->monitors->removeElement($monitor);
        }

        return $this;
    }

    /**
     * @return Collection|SoftwareImplementation[]
     */
    public function getImplementations(): Collection {
        return $this->implementations;
    }

    public function addImplementation(SoftwareImplementation $implementation): self {
        if (!$this->implementations->contains($implementation)) {
            $this->implementations[] = $implementation;
            $implementation->setLicences($this);
        }

        return $this;
    }

    public function removeImplementation(SoftwareImplementation $implementation): self {
        if ($this->implementations->contains($implementation)) {
            $this->implementations->removeElement($implementation);
            // set the owning side to null (unless already changed)
            if ($implementation->getLicences() === $this) {
                $implementation->setLicences(null);
            }
        }

        return $this;
    }

}
