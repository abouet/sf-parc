<?php

namespace App\Entity\Hardware;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Core\BaseProperty;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\StatusRepository")
 */
class Status extends BaseProperty implements InheritanceTableInterface {

    /**
     * @ORM\OneToMany(targetEntity="Device", mappedBy="status")
     */
    protected $devices;

    public function __construct()
    {
        $this->devices = new ArrayCollection();
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setStatus($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getStatus() === $this) {
                $device->setStatus(null);
            }
        }

        return $this;
    }

}
