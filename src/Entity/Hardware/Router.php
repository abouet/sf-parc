<?php

namespace App\Entity\Hardware;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\RouterRepository")
 */
class Router {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=38)
     */
    protected $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $mac;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isIpV6;

    public function getIp(): ?string {
        return $this->ip;
    }

    public function setIp(string $ip): self {
        $this->ip = $ip;

        return $this;
    }

    public function getMac(): ?string {
        return $this->mac;
    }

    public function setMac(string $mac): self {
        $this->mac = $mac;

        return $this;
    }

    public function IsIpV6(): ?bool {
        return $this->isIp6;
    }

    public function setIpV6(bool $bool = true): self {
        $this->isIpV6 = $bool;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsIpV6(): ?bool
    {
        return $this->isIpV6;
    }

    public function setIsIpV6(bool $isIpV6): self
    {
        $this->isIpV6 = $isIpV6;

        return $this;
    }

}
