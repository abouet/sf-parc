<?php

namespace App\Entity\Hardware;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\ScreenRepository")
 */
class Screen extends Device implements DeviceInterface, InheritanceTableInterface {

    /**
     * @ORM\Column(type="smallint")
     */
    protected $size;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isTactile;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $hasMicro;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $hasSpeaker;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $networkAttached;

    /**
     * A computer may have several screens attached
     * 
     * @ORM\ManyToMany(targetEntity="Computer", mappedBy="monitors")
     */
    protected $computers;

    public function __construct() {
        Parent::__construct();
        $this->computers = new ArrayCollection();
    }

    public function getSize(): ?int {
        return $this->size;
    }

    public function setSize(int $size): self {
        $this->size = $size;

        return $this;
    }

    public function hasMicro(): ?bool {
        return $this->hasMicro;
    }

    public function setHasMicro(bool $bool = true): self {
        $this->hasMicro = $bool;

        return $this;
    }

    public function hasSpeaker(): ?bool {
        return $this->hasSpeaker;
    }

    public function setHasSpeaker(bool $bool = true): self {
        $this->hasSpeaker = $bool;

        return $this;
    }

    public function isNetworkAttached(): ?bool {
        return $this->networkAttached;
    }

    public function setNetworkAttached(bool $bool = true): self {
        $this->networkAttached = $bool;

        return $this;
    }

    public function isTactile(): ?bool {
        return $this->isTactile;
    }

    public function setTactile(bool $bool = true): self {
        $this->isTactile = $bool;

        return $this;
    }

    public function getIsTactile(): ?bool
    {
        return $this->isTactile;
    }

    public function setIsTactile(bool $isTactile): self
    {
        $this->isTactile = $isTactile;

        return $this;
    }

    public function getHasMicro(): ?bool
    {
        return $this->hasMicro;
    }

    public function getHasSpeaker(): ?bool
    {
        return $this->hasSpeaker;
    }

    public function getNetworkAttached(): ?bool
    {
        return $this->networkAttached;
    }

    /**
     * @return Collection|Computer[]
     */
    public function getComputers(): Collection
    {
        return $this->computers;
    }

    public function addComputer(Computer $computer): self
    {
        if (!$this->computers->contains($computer)) {
            $this->computers[] = $computer;
            $computer->addMonitor($this);
        }

        return $this;
    }

    public function removeComputer(Computer $computer): self
    {
        if ($this->computers->contains($computer)) {
            $this->computers->removeElement($computer);
            $computer->removeMonitor($this);
        }

        return $this;
    }

}
