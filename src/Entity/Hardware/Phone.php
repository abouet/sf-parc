<?php

namespace App\Entity\Hardware;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\PhoneRepository")
 */
class Phone extends Device implements DeviceInterface, InheritanceTableInterface {

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $number;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $ext;

    public function getNumber(): ?int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    public function getExt(): ?int {
        return $this->ext;
    }

    public function setExt(?int $ext): self {
        $this->ext = $ext;

        return $this;
    }

}
