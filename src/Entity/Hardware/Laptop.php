<?php

namespace App\Entity\Hardware;

/**
 * Description of Latop
 * 
 */
class Laptop extends Computer {

    public function __construct() {
        Parent::__construct();
        $this->isLaptop = true;
        $this->isTablet = false;
    }

}
