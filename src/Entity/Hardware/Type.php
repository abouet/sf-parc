<?php

namespace App\Entity\Hardware;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="hardware_type")
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\TypeRepository")
 * @Gedmo\TranslationEntity(class="Entity\Hardware\TypeTranslation")
 */
class Type {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * to restricted to appropriate device type (Phone, server, computer...)
     * 
     * @ORM\Column(type="string", length=20)
     */
    private $class;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\OneToMany(
     *   targetEntity="TypeTranslation",
     *   mappedBy="type",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="Device", mappedBy="type")
     */
    protected $devices;

    public function __construct() {
        $this->translations = new ArrayCollection();
        $this->devices = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getLabel(): ?string {
        return $this->label;
    }

    public function setLabel($label): self {
        $this->label = $label;

        return $this;
    }

    public function setDescription($description): self {
        $this->description = $description;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function getCreated(): ?\DateTimeInterface {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self {
        $this->updated = $updated;

        return $this;
    }

    public function getTranslations() {
        return $this->translations;
    }

    public function addTranslation(TypeTranslation $t) {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function __toString(): string {
        return (string) $this->getlabel();
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function removeTranslation(TypeTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getType() === $this) {
                $translation->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setType($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getType() === $this) {
                $device->setType(null);
            }
        }

        return $this;
    }

}
