<?php

namespace App\Entity\Hardware;

use App\Entity\Company\Manufacturer;
use App\Entity\Core\OrganisationUnit;
use App\Entity\Person\Technician;
use App\Entity\Person\User;
use Doctrine\ORM\Mapping as ORM;
 use\Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Core\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\DeviceRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="class", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *      "": "Device",
 *      "Computer": "Computer",
 *      "Phone": "Phone",
 *      "Printer": "Printer",
 *      "Router": "Router",
 *      "Screen": "Screen",
 *      "Server": "Server"
 * })
 */
class Device extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $serialNumber;

    /**
     * Inventory Reference
     * 
     * @ORM\Column(type="string", length=50)
     */
    protected $inventoryKey;

    /**
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * date of purchase
     * 
     * @ORM\Column(type="date")
     */
    protected $purchaseDate;

    /**
     * date of first installation
     * 
     * @ORM\Column(type="date")
     */
    protected $installationDate;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="devices")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="devices")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\OrganisationUnit", inversedBy="devices")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     */
    protected $unit;
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="devices")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Manufacturer", inversedBy="devices")
     * @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     */
    protected $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="Model", inversedBy="devices")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    protected $model;

    /**
     * A device/appliance is looked after by one technician
     * 
     * @see App\Entity\Person\Technician::$appliances
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\Technician", inversedBy="appliances")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id")
     */
    protected $technician;

    /**
     * A device/appliance is granted to a user
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\User", inversedBy="devices")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    static function getTypes() {
        $types = array();
        $type[Commputer::getClassInformation()->name] = Commputer::getClassInformation()->label;
        $type[Server::getClassInformation()->name] = Server::getClassInformation()->label;
        /* $types[AppliancePeer::CLASSKEY_COMPUTER] = Computer::getClassLabel();
          $types[AppliancePeer::CLASSKEY_MONITOR]  = Monitor::getClassLabel();
          $types[AppliancePeer::CLASSKEY_PHONE]    = Phone::getClassLabel();
          $types[AppliancePeer::CLASSKEY_PRINTER]  = Printer::getClassLabel();
          $types[AppliancePeer::CLASSKEY_ROUTER]   = Router::getClassLabel();
          $types[AppliancePeer::CLASSKEY_SERVER]   = Server::getClassLabel();
          $types[AppliancePeer::CLASSKEY_NETWORKSWITCH] = NetworkSwitch::getClassLabel(); */
        return $types;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string {
        return $this->desc;
    }

    public function setDescription(string $desc): self {
        $this->description = $desc;
        return $this;
    }

    public function getSerialNumber(): ?string {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $sn): self {
        $this->serialNumber = $sn;
        return $this;
    }

    public function getInventoryKey(): ?string {
        return $this->inventoryKey;
    }

    public function setInventoryKey(string $key): self {
        $this->inventoryKey = $key;
        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;
        return $this;
    }

    public function getPurchaseDate(): ?\DateTime {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(\DateTimeInterface $date): self {
        $this->purchaseDate = $date;
        return $this;
    }

    public function getInstallationDate(): ?\DateTime {
        return $this->installationDate;
    }

    public function setInstallationDate(\DateTimeInterface $date): self {
        $this->installationDate = $date;
        return $this;
    }

    public function __toString(): string {
        return (string) $this->getName();
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUnit(): ?OrganisationUnit
    {
        return $this->unit;
    }

    public function setUnit(?OrganisationUnit $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function setModel(?Model $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getTechnician(): ?Technician
    {
        return $this->technician;
    }

    public function setTechnician(?Technician $technician): self
    {
        $this->technician = $technician;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}
