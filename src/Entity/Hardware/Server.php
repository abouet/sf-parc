<?php

namespace App\Entity\Hardware;

use Doctrine\ORM\Mapping as ORM;
 use\Doctrine\Common\Collections\ArrayCollection;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Hardware\ServerRepository")
 */
class Server extends Computer {

    /**
     * @ORM\Column(type="string", length=38, unique=true)
     */
    protected $ip;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isIpV6;

    public function __construct() {
        Parent::__construct();
        $this->licenses = new ArrayCollection();
        $this->softwares = new ArrayCollection();
    }

    public function getDomain(): ?string {
        return $this->domain;
    }

    public function setDomain(?string $domain): self {
        $this->domain = $domain;

        return $this;
    }

    public function getIp(): ?string {
        return $this->ip;
    }

    public function setIp(string $ip): self {
        $this->ip = $ip;

        return $this;
    }

    public function IsIpV6(): ?bool {
        return $this->isIp6;
    }

    public function setIpV6(bool $bool = true): self {
        $this->isIpV6 = $bool;

        return $this;
    }

}
