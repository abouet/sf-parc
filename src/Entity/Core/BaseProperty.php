<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\BasePropertyRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string")
 * @ORM\DiscriminatorMap({
 *      "Group": "App\Entity\Hardware\Group",
 *      "Status": "App\Entity\Hardware\Status",
 *      "Platform": "App\Entity\Software\Platform",
 *      "Category": "App\Entity\Software\Category"
 * })
 */
abstract class BaseProperty extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=20)
     */
    protected $label;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    protected $is_public;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    protected $color;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    protected $textColor;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $seq;

    /**
     * @ORM\OneToMany(
     *   targetEntity="PropertyTranslation",
     *   mappedBy="property",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getLabel(): ?string {
        return $this->label;
    }

    public function setLabel(?string $label): self {
        $this->label = $label;
        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $desc): self {
        $this->description = $desc;
        return $this;
    }

    public function isPublic(): ?bool {
        return $this->is_public;
    }

    public function setPublic(bool $bool = true): self {
        $this->is_public = $bool;

        return $this;
    }

    public function getColor(): ?string {
        return $this->color;
    }

    public function setColor(?string $color): self {
        $this->color = $color;

        return $this;
    }

    public function getTextColor(): ?string {
        return $this->textColor;
    }

    public function setTextColor(?string $textColor): self {
        $this->textColor = $textColor;

        return $this;
    }

    public function getOrder(): ?int {
        return $this->seq;
    }

    public function setOrder(int $seq): self {
        $this->seq = $seq;

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getLabel();
    }

    public function getIsPublic(): ?bool
    {
        return $this->is_public;
    }

    public function setIsPublic(bool $is_public): self
    {
        $this->is_public = $is_public;

        return $this;
    }

    public function getSeq(): ?int
    {
        return $this->seq;
    }

    public function setSeq(int $seq): self
    {
        $this->seq = $seq;

        return $this;
    }

    /**
     * @return Collection|PropertyTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(PropertyTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProperty($this);
        }

        return $this;
    }

    public function removeTranslation(PropertyTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProperty() === $this) {
                $translation->setProperty(null);
            }
        }

        return $this;
    }

}
