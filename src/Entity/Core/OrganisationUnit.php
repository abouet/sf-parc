<?php

namespace App\Entity\Core;

use App\Entity\Hardware\Device;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Entity\Core\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="App\Repository\Core\OrganisationUnitRepository")
 */
class OrganisationUnit extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="devices")
     * ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hardware\Device", mappedBy="unit")
     */
    protected $devices;

    public function __construct() {
        $this->devices = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getName();
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setUnit($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getUnit() === $this) {
                $device->setUnit(null);
            }
        }

        return $this;
    }

}
