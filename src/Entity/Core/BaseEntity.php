<?php

namespace App\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Person\User;

/**
 * Description of BaseEntity
 *
 * ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class BaseEntity {

    const CREATION_DATE = 'creationDate';
    const CREATION_USER = 'creationUser';
    const UPDATE_DATE = 'updateDate';
    const UPDATE_USER = 'updateUser';

    /**
     * Creation date
     * 
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * User who creates the record
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $creationUser;

    /**
     * Date on which the record was updated
     * 
     * @ORM\Column(type="datetime")
     */
    protected $updateDate;

    /**
     * User who updates the record
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * 
     */
    protected $updateUser;

    public function getCreationDate(): \DateTimeInterface {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $date): self {
        $this->creationDate = $date;
        return $this;
    }

    public function getCreationUser(): User {
        return $this->creationUser;
    }

    public function getUpdateDate(): ?\DateTimeInterface {
        return $this->updateDate;
    }

    public function setUpdateDate(\DateTimeInterface $date): self {
        $this->updateDate = $date;
        return $this;
    }

    public function getUpdateUser(): ?User {
        return $this->updateUser;
    }

}
