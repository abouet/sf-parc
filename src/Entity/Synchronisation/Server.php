<?php

namespace App\Entity\Synchronisation;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Core\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Synchronisation\ServerRepository")
 * @ORM\table(name="sync_server")
 */
abstract class Server extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=38)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * auto refresh frequency (cron format)
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $frequency;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(name="sync_date", type="datetime", nullable=true)
     */
    private $synchronisationDate;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getIp(): ?string {
        return $this->ip;
    }

    public function setIp(string $ip): self {
        $this->ip = $ip;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getFrequency(): ?string {
        return $this->frequency;
    }

    public function setFrequency(?string $frequency): self {
        $this->frequency = $frequency;

        return $this;
    }

    public function getUrl(): ?string {
        return $this->url;
    }

    public function setUrl(?string $url): self {
        $this->url = $url;

        return $this;
    }

    public function getSynchronisationDate(): ?\DateTimeInterface {
        return $this->synchronisationDate;
    }

    public function setSynchronisationDate(?\DateTimeInterface $dateTime): self {
        $this->synchronisationDate = $dateTime;

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getName();
    }

}
