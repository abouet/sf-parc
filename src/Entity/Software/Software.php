<?php

namespace App\Entity\Software;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\OSRepository")
 */
class Software extends AbstractSoftware implements InheritanceTableInterface, SystemInterface {

    /**
     * @ORM\OneToMany(targetEntity="Version", mappedBy="software")
     */
    protected $versions;

    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return Collection|Version[]
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(Version $version): self
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setSoftware($this);
        }

        return $this;
    }

    public function removeVersion(Version $version): self
    {
        if ($this->versions->contains($version)) {
            $this->versions->removeElement($version);
            // set the owning side to null (unless already changed)
            if ($version->getSoftware() === $this) {
                $version->setSoftware(null);
            }
        }

        return $this;
    }

}
