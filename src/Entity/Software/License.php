<?php

namespace App\Entity\Software;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\LicenseRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *      "OpenSource": "OpenSource",
 *      "OEM": "OEM",
 *      "Freeware": "Freeware",
 *      "Editor": "EditorLicense",
 *      "": "License"
 * })
 */
class License {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $SerialNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $expiryDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isFree;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\OneToMany(targetEntity="SoftwareImplementation", mappedBy="licences")
     */
    protected $implementations;

    public function __construct()
    {
        $this->implementations = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getSerialNumber(): ?string {
        return $this->SerialNumber;
    }

    public function setSerialNumber(?string $SerialNumber): self {
        $this->SerialNumber = $SerialNumber;

        return $this;
    }

    public function getExpiryDate(): ?\DateTimeInterface {
        return $this->expiryDate;
    }

    public function setExpiryDate(?\DateTimeInterface $expiryDate): self {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    public function isFree(): ?bool {
        return $this->isFree;
    }

    public function setFree(bool $bool = true): self {
        $this->isFree = $bool;

        return $this;
    }

    public function getNote(): ?string {
        return $this->note;
    }

    public function setNote(?string $note): self {
        $this->note = $note;

        return $this;
    }

    public function getSoftware(): Software {
        return $this->software;
    }

    public function setSoftware(Software $software): self {
        $this->software = $software;
        return $this;
    }

    /**
     * @return Collection|SoftwareImplementation[]
     */
    public function getImplementations(): Collection
    {
        return $this->implementations;
    }

    public function addImplementation(SoftwareImplementation $implementation): self
    {
        if (!$this->implementations->contains($implementation)) {
            $this->implementations[] = $implementation;
            $implementation->setLicences($this);
        }

        return $this;
    }

    public function removeImplementation(SoftwareImplementation $implementation): self
    {
        if ($this->implementations->contains($implementation)) {
            $this->implementations->removeElement($implementation);
            // set the owning side to null (unless already changed)
            if ($implementation->getLicences() === $this) {
                $implementation->setLicences(null);
            }
        }

        return $this;
    }

}
