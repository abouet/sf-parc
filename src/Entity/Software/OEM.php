<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\OEMRepository")
 */
class OEM extends License implements InheritanceTableInterface {

    private $licence;

}
