<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\VersionRepository")
 */
class Version extends AbstractVersion implements InheritanceTableInterface {

    /**
     * Date on which the version will not be supported anymore
     * 
     * @ORM\Column(type="date")
     */
    protected $endSupport;

    /**
     * @ORM\ManyToOne(targetEntity="Software", inversedBy="versions")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $software;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Software\SoftwareImplementation", mappedBy="software")
     */
    protected $implementations;

    public function getEndSupport(): ?\DateTimeInterface {
        return $this->endSupport;
    }

    public function setEndSupport(\DateTimeInterface $date): self {
        $this->endSupport = $date;
        return $this;
    }

    public function getSoftware(): Software {
        return $this->software;
    }

    public function setSoftware(Software $software): self {
        $this->software = $software;
        return $this;
    }

    /**
     * @return Collection|SoftwareImplementation[]
     */
    public function getImplementations(): Collection {
        return $this->implementations;
    }

    public function addImplementation(SoftwareImplementation $implementation): self {
        if (!$this->implementations->contains($implementation)) {
            $this->implementations[] = $implementation;
            $implementation->setLicences($this);
        }

        return $this;
    }

    public function removeImplementation(SoftwareImplementation $implementation): self {
        if ($this->implementations->contains($implementation)) {
            $this->implementations->removeElement($implementation);
            // set the owning side to null (unless already changed)
            if ($implementation->getLicences() === $this) {
                $implementation->setLicences(null);
            }
        }

        return $this;
    }

}
