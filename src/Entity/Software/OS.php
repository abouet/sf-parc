<?php

namespace App\Entity\Software;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\OSRepository")
 */
class OS extends AbstractSoftware implements InheritanceTableInterface, SystemInterface {

    /**
     * @ORM\OneToMany(targetEntity="OSVersion", mappedBy="OS")
     */
    protected $versions;

    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return Collection|OSVersion[]
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(OSVersion $version): self
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setOS($this);
        }

        return $this;
    }

    public function removeVersion(OSVersion $version): self
    {
        if ($this->versions->contains($version)) {
            $this->versions->removeElement($version);
            // set the owning side to null (unless already changed)
            if ($version->getOS() === $this) {
                $version->setOS(null);
            }
        }

        return $this;
    }

}
