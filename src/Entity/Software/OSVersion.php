<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\OSVersionRepository")
 */
class OSVersion extends AbstractVersion implements InheritanceTableInterface, SystemInterface {

    /**
     * @ORM\ManyToOne(targetEntity="OS", inversedBy="versions")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $OS;

    public function getOs(): OS {
        return $this->OS;
    }

    public function setOS(OS $os): self {
        $this->OS = $os;
        return $this;
    }

}
