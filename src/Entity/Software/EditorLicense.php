<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\LicenseRepository")
 */
class EditorLicense extends License implements InheritanceTableInterface {

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $number;

    public function __construct() {
        $this->setFree(false);
    }

    public function getAuthorizedImplementations(): ?int {
        return $this->number;
    }

    public function setAuthorizedImplementations(?int $nbr): self {
        $this->number = $nbr;

        return $this;
    }

    public function countImplementations(): ?int {
        return count($this->getImplementations());
    }

}
