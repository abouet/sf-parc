<?php

namespace App\Entity\Software;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Core\BaseProperty;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\CategoryRepository")
 */
class Category extends BaseProperty implements InheritanceTableInterface {

    /**
     * @ORM\OneToMany(targetEntity="Software", mappedBy="category")
     */
    protected $softwares;

    public function __construct()
    {
        $this->softwares = new ArrayCollection();
    }

    /**
     * @return Collection|Software[]
     */
    public function getSoftwares(): Collection
    {
        return $this->softwares;
    }

    public function addSoftware(Software $software): self
    {
        if (!$this->softwares->contains($software)) {
            $this->softwares[] = $software;
            $software->setCategory($this);
        }

        return $this;
    }

    public function removeSoftware(Software $software): self
    {
        if ($this->softwares->contains($software)) {
            $this->softwares->removeElement($software);
            // set the owning side to null (unless already changed)
            if ($software->getCategory() === $this) {
                $software->setCategory(null);
            }
        }

        return $this;
    }

}
