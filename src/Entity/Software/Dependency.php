<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\DependencyRepository")
 */
class Dependency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $versionContraint;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVersionContraint(): ?string
    {
        return $this->versionContraint;
    }

    public function setVersionContraint(?string $versionContraint): self
    {
        $this->versionContraint = $versionContraint;

        return $this;
    }
}
