<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\SoftwareImplementationRepository")
 */
final class SoftwareImplementation {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hardware\Computer", inversedBy="implementations")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $computer;

    /**
     * @ORM\ManyToOne(targetEntity="License", inversedBy="implementations")
     * @ORM\JoinColumn(name="license_id", referencedColumnName="id")
     */
    private $licences;

    /**
     * @ORM\ManyToOne(targetEntity="Version", inversedBy="implementations")
     * @ORM\JoinColumn(name="software_id", referencedColumnName="id")
     */
    private $software;

    /**
     * Daemon/application communication port
     * 
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $port;

    public function getId(): ?int {
        return $this->id;
    }

    public function getSoftware(): Software {
        return $this->software;
    }

    public function setSoftware(Software $software): self {
        $this->software = $software;
        return $this;
    }

    public function getPort(): ?int {
        return $this->port;
    }

    public function setPort(?int $port): self {
        $this->port = $port;

        return $this;
    }

}
