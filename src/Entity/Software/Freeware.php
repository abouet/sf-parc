<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\FreewareRepository")
 */
class Freeware extends License implements InheritanceTableInterface
{
    public function __construct() {
        parent::__construct();
        $this->setFree();
    }
}
