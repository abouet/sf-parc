<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\InheritanceTableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\ServicePackRepository")
 */
class ServicePack extends AbstractSoftware implements InheritanceTableInterface, SystemInterface {
    
}
