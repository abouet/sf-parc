<?php

namespace App\Entity\Software;

use Doctrine\ORM\Mapping as ORM;

class AbstractVersion extends AbstractSoftware {

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isLTS;

    /**
     * Date on which the version was actually released
     * 
     * @ORM\Column(type="date")
     */
    protected $release;

    /**
     * Date on which the version is normally due
     * 
     * @ORM\Column(type="date")
     */
    protected $planned;

    public function isLTS(): ?bool {
        return $this->isLTS;
    }

    public function setLTS(bool $bool = true): self {
        $this->isLTS = $bool;
        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface {
        return $this->release;
    }

    public function setReleaseDate(\DateTimeInterface $release): self {
        $this->release = $release;
        return $this;
    }

    public function getPlannedDate(): ?\DateTimeInterface {
        return $this->planned;
    }

    public function setPlannedDate(\DateTimeInterface $planned): self {
        $this->planned = $planned;
        return $this;
    }

}
