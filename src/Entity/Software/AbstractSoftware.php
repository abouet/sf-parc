<?php

namespace App\Entity\Software;

use App\Entity\Company\Editor;
use Doctrine\ORM\Mapping as ORM;
 use\Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Core\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Software\SoftwareRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *      "Software": "Software",
 *      "Version": "Version",
 *      "OS": "OS",
 *      "OSVersion": "OSVersion",
 *      "SP": "ServicePack",
 *      "Daemon": "Daemon"
 * })
 */
abstract class AbstractSoftware extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * Default port
     * 
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $port;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $obsolete;

    /**
     * @ORM\ManyToOne(targetEntity="Platform", inversedBy="softwares")
     * @ORM\JoinColumn(name="plateform_id", referencedColumnName="id")
     */
    protected $platform;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Editor", inversedBy="softwares")
     * @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     */
    protected $editor;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="softwares")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getNote(): ?string {
        return $this->note;
    }

    public function setNote(?string $note): self {
        $this->note = $note;

        return $this;
    }

    public function getPort(): ?int {
        return $this->port;
    }

    public function setPort(?int $port): self {
        $this->port = $port;

        return $this;
    }

    public function getPlatform(): ?Platform {
        return $this->platform;
    }

    public function setPlatform(?Platform $platform): self {
        $this->platform = $platform;

        return $this;
    }

    public function getEditor(): ?Editor {
        return $this->editor;
    }

    public function setEditor(?Editor $editor): self {
        $this->editor = $editor;

        return $this;
    }

    public function getCategory(): ?Category {
        return $this->category;
    }

    public function setCategory(?Category $category): self {
        $this->category = $category;

        return $this;
    }

}
