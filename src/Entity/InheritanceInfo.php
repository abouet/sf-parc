<?php

namespace App\Entity;

use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor;

/**
 * Description of SingleInheritanceInfo
 *
 * @author abouet
 */
trait InheritanceInfo {

    static public function getClassInformation(): \stdClass {
        $class = new \stdClass();

        /*$reflectionExtractor = new ReflectionExtractor();
        $PhpDocExtractor = new PhpDocExtractor();
        $doctrineExtractor = new DoctrineExtractor();
        $propertyInfo = new PropertyInfoExtractor(
                [
            $PhpDocExtractor,
            $reflectionExtractor,
            $doctrineExtractor
                ]
        );
        $class->shortName = substr(parent::class, strrpos(parent::class, '\\') + 1);
        $class->label = strtolower($class->name . '.label');        
        $title = $propertyInfo->getShortDescription($class, $property);
        $paragraph = $propertyInfo->getLongDescription($class, $property);*/

        return $class;
    }
}
