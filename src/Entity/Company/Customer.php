<?php

namespace App\Entity\Company;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\CustomerRepository")
 */
class Customer extends AbstractCompany implements OrganisationUnitInterface {

    public function __construct() {
        parent::_construct();
        $this->setCustomer();
    }

    public function isCustomer(): bool {
        return true;
    }

}
