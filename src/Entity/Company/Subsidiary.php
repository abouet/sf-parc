<?php

namespace App\Entity\Company;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\SubsidiaryRepository")
 */
class Subsidiary extends Company implements OrganisationUnitInterface{

    /**
     * ORM\OneToOne(targetEntity="AbstractCompany")
     * ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    public function __construct() {
        parent::_construct();
        $this->setSubsidiary();
    }

    public function getParent(): Company {
        return $this->parent;
    }

    public function setParent(Company $cie): self {
        $this->parent = $cie;
        return $this;
    }

    public function isSubsidiary(): bool {
        return true;
    }

}
