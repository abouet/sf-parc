<?php

namespace App\Entity\Company;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Core\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\CompanyRepository")
 */
abstract class AbstractCompany extends BaseEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $website;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isCustomer;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isSubsiary;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isEditor;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isInternal;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isManufacturer;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isSupplier;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string {
        return $this->website;
    }

    public function setWebsite(?string $website): self {
        $this->website = $website;

        return $this;
    }

    public function getNote(): ?string {
        return $this->note;
    }

    public function setNote(?string $note): self {
        $this->note = $note;

        return $this;
    }

    public function isCustomer(): bool {
        return $this->isCustomer;
    }

    public function setCustomer(bool $bool = true): self {
        $this->isCustomer = $bool;
        return $this;
    }

    public function isEditor(): bool {
        return $this->isEditor;
    }

    public function setEditor(bool $bool = true): self {
        $this->isEditor = $bool;
        return $this;
    }

    public function isInternal(): bool {
        return $this->isInternal;
    }

    public function setInternal(bool $bool = true): self {
        $this->isInternal = $bool;
        return $this;
    }

    public function isManufacturer(): bool {
        return $this->isManufacturer;
    }

    public function setManufacturer(bool $bool = true): self {
        $this->isManufacturer = $bool;
        return $this;
    }

    public function isSubsidiary(): bool {
        return $this->isSubsiary;
    }

    public function setSubsidiary(bool $bool = true): self {
        $this->isSubsiary = $bool;
        return $this;
    }

    public function isSupplier(): bool {
        return $this->isSupplier;
    }

    public function setSupplier(bool $bool = true): self {
        $this->isSupplier = $bool;
        return $this;
    }

    public function __toString(): string {
        return (string) $this->getName();
    }

    public function getIsCustomer(): ?bool
    {
        return $this->isCustomer;
    }

    public function setIsCustomer(bool $isCustomer): self
    {
        $this->isCustomer = $isCustomer;

        return $this;
    }

    public function getIsSubsiary(): ?bool
    {
        return $this->isSubsiary;
    }

    public function setIsSubsiary(bool $isSubsiary): self
    {
        $this->isSubsiary = $isSubsiary;

        return $this;
    }

    public function getIsEditor(): ?bool
    {
        return $this->isEditor;
    }

    public function setIsEditor(bool $isEditor): self
    {
        $this->isEditor = $isEditor;

        return $this;
    }

    public function getIsInternal(): ?bool
    {
        return $this->isInternal;
    }

    public function setIsInternal(bool $isInternal): self
    {
        $this->isInternal = $isInternal;

        return $this;
    }

    public function getIsManufacturer(): ?bool
    {
        return $this->isManufacturer;
    }

    public function setIsManufacturer(bool $isManufacturer): self
    {
        $this->isManufacturer = $isManufacturer;

        return $this;
    }

    public function getIsSupplier(): ?bool
    {
        return $this->isSupplier;
    }

    public function setIsSupplier(bool $isSupplier): self
    {
        $this->isSupplier = $isSupplier;

        return $this;
    }

}
