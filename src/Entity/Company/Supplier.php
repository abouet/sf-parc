<?php

namespace App\Entity\Company;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\SupplierRepository")
 */
class Supplier extends AbstractCompany {

    public function __construct() {
        parent::_construct();
        $this->setSupplier();
    }

    public function isSupplier(): bool {
        return true;
    }

}
