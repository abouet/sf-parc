<?php

namespace App\Entity\Company;

use App\Entity\Software\Software;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\EditorRepository")
 */
class Editor extends AbstractCompany {

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Software\Software", mappedBy="editor")
     */
    protected $softwares;

    public function __construct() {
        parent::_construct();
        $this->setEditor();
        $this->softwares = new ArrayCollection();
    }

    public function isEditor(): bool {
        return true;
    }

    /**
     * @return Collection|Software[]
     */
    public function getSoftwares(): Collection
    {
        return $this->softwares;
    }

    public function addSoftware(Software $software): self
    {
        if (!$this->softwares->contains($software)) {
            $this->softwares[] = $software;
            $software->setEditor($this);
        }

        return $this;
    }

    public function removeSoftware(Software $software): self
    {
        if ($this->softwares->contains($software)) {
            $this->softwares->removeElement($software);
            // set the owning side to null (unless already changed)
            if ($software->getEditor() === $this) {
                $software->setEditor(null);
            }
        }

        return $this;
    }

}
