<?php

namespace App\Entity\Company;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\CompanyRepository")
 */
class Company extends AbstractCompany implements OrganisationUnitInterface {

    public function __construct() {
        parent::_construct();
        $this->setInternal();
    }

    public function isInternal(): bool {
        return true;
    }

}
