<?php

namespace App\Entity\Company;

use App\Entity\Hardware\Device;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\ManufacturerRepository")
 */
class Manufacturer extends AbstractCompany {

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hardware\Device", mappedBy="manufacturer")
     */
    protected $devices;

    public function __construct() {
        parent::_construct();
        $this->setManufacturer();
        $this->devices = new ArrayCollection();
    }

    public function isManufacturer(): bool {
        return true;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setManufacturer($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getManufacturer() === $this) {
                $device->setManufacturer(null);
            }
        }

        return $this;
    }

}
