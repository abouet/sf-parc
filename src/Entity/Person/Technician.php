<?php

namespace App\Entity\Person;

use App\Entity\Hardware\Device;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Person\TechnicianRepository")
 */
class Technician extends User {

    const ROLES = ["ROLE_USER", "ROLE_TECHNICIAN"];

    /**
     * A technician is in charge of many devices
     * property "devices" is not used as an error occurs when validating the schema
     * given Technician inheritance from User
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Hardware\Device", mappedBy="technician")
     */
    protected $appliances;

    public function __construct() {
        parent::__construct();
        $this->setTechnician();
        $this->appliances = new ArrayCollection();
    }

    public function isTechnician(): ?bool {
        return true;
    }

    /**
     * @return Collection|Device[]
     */
    public function getAppliances(): Collection
    {
        return $this->appliances;
    }

    public function addAppliance(Device $appliance): self
    {
        if (!$this->appliances->contains($appliance)) {
            $this->appliances[] = $appliance;
            $appliance->setTechnician($this);
        }

        return $this;
    }

    public function removeAppliance(Device $appliance): self
    {
        if ($this->appliances->contains($appliance)) {
            $this->appliances->removeElement($appliance);
            // set the owning side to null (unless already changed)
            if ($appliance->getTechnician() === $this) {
                $appliance->setTechnician(null);
            }
        }

        return $this;
    }

}









