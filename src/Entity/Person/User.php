<?php

namespace App\Entity\Person;

use App\Entity\Hardware\Device;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Person\UserRepository")
 */
class User extends BaseUser {

    const ROLES = ["ROLE_USER"];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $culture;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $ext;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $mobile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $signature;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $technician;

    /**
     * One user has many preferences.
     * 
     * @ORM\OneToMany(targetEntity="UserPreference", mappedBy="user")
     */
    protected $preferences;

    /**
     * A user is granted many devices
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Hardware\Device", mappedBy="user")
     */
    protected $devices;

    public function __construct() {
        parent::__construct();
        $this->preferences = new ArrayCollection();
        $this->devices = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self {
        $this->firstname = $firstname;

        return $this;
    }

    public function getFullname(): ?string {
        return $this->getFirstname() . ' ' . $this->getName();
    }

    public function getCulture(): ?string {
        return $this->culture;
    }

    public function setCulture(?string $culture): self {
        $this->culture = $culture;

        return $this;
    }

    public function getPhone(): ?int {
        return $this->phone;
    }

    public function setPhone(?int $phone): self {
        $this->phone = $phone;

        return $this;
    }

    public function getExt(): ?int {
        return $this->ext;
    }

    public function setExt(?int $ext): self {
        $this->ext = $ext;

        return $this;
    }

    public function getMobile(): ?int {
        return $this->mobile;
    }

    public function setMobile(?int $mobile): self {
        $this->mobile = $mobile;

        return $this;
    }

    public function getSignature(): ?string {
        return $this->signature;
    }

    public function setSignature(?string $signature): self {
        $this->signature = $signature;

        return $this;
    }

    public function getNotes(): ?string {
        return $this->notes;
    }

    public function setNotes(?string $notes): self {
        $this->notes = $notes;

        return $this;
    }

    public function isTechnician(): ?bool {
        return $this->technician;
    }

    public function setTechnician(bool $tech = true): self {
        $this->technician = $tech;

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getFullname();
    }

    public function getTechnician(): ?bool
    {
        return $this->technician;
    }

    /**
     * @return Collection|UserPreference[]
     */
    public function getPreferences(): Collection
    {
        return $this->preferences;
    }

    public function addPreference(UserPreference $preference): self
    {
        if (!$this->preferences->contains($preference)) {
            $this->preferences[] = $preference;
            $preference->setUser($this);
        }

        return $this;
    }

    public function removePreference(UserPreference $preference): self
    {
        if ($this->preferences->contains($preference)) {
            $this->preferences->removeElement($preference);
            // set the owning side to null (unless already changed)
            if ($preference->getUser() === $this) {
                $preference->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setUser($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getUser() === $this) {
                $device->setUser(null);
            }
        }

        return $this;
    }

}
