<?php

namespace App\Repository\Software;

use App\Entity\Software\Freeware;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Freeware|null find($id, $lockMode = null, $lockVersion = null)
 * @method Freeware|null findOneBy(array $criteria, array $orderBy = null)
 * @method Freeware[]    findAll()
 * @method Freeware[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FreewareRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Freeware::class);
    }

    // /**
    //  * @return Freeware[] Returns an array of Freeware objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Freeware
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
