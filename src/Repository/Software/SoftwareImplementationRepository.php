<?php

namespace App\Repository\Software;

use App\Entity\Software\SoftwareImplementation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SoftwareImplementation|null find($id, $lockMode = null, $lockVersion = null)
 * @method SoftwareImplementation|null findOneBy(array $criteria, array $orderBy = null)
 * @method SoftwareImplementation[]    findAll()
 * @method SoftwareImplementation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoftwareImplementationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SoftwareImplementation::class);
    }

    // /**
    //  * @return SoftwareImplementation[] Returns an array of SoftwareImplementation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SoftwareImplementation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
