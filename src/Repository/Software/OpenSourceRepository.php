<?php

namespace App\Repository\Software;

use App\Entity\Software\OpenSource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OpenSource|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenSource|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenSource[]    findAll()
 * @method OpenSource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenSourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenSource::class);
    }

    // /**
    //  * @return OpenSource[] Returns an array of OpenSource objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OpenSource
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
