<?php

namespace App\Repository\Software;

use App\Entity\Software\ServicePack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ServicePack|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServicePack|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServicePack[]    findAll()
 * @method ServicePack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServicePackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServicePack::class);
    }

    // /**
    //  * @return ServicePack[] Returns an array of ServicePack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ServicePack
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
