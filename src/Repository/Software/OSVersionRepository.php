<?php

namespace App\Repository\Software;

use App\Entity\Software\OSVersion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OSVersion|null find($id, $lockMode = null, $lockVersion = null)
 * @method OSVersion|null findOneBy(array $criteria, array $orderBy = null)
 * @method OSVersion[]    findAll()
 * @method OSVersion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OSVersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OSVersion::class);
    }

    // /**
    //  * @return OSVersion[] Returns an array of OSVersion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OSVersion
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
