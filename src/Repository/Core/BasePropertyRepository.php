<?php

namespace App\Repository\Core;

use App\Entity\Core\BaseProperty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseProperty|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseProperty|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseProperty[]    findAll()
 * @method BaseProperty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasePropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseProperty::class);
    }

    // /**
    //  * @return BaseProperty[] Returns an array of BaseProperty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseProperty
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
