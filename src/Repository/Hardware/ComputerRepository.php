<?php

namespace App\Repository\Hardware;

use App\Entity\Hardware\Computer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Computer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Computer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Computer[]    findAll()
 * @method Computer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComputerRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Computer::class);
    }

    public function findLaptops() {
        return $this->getEntityManager->createQueryBuilder()
                        ->select('l')
                        ->from(\App\Entity\Hardware\Laptop::class, 'l')
                        ->andWhere('l.isLaptop = true')
                        ->getQuery()
                        ->getResult();
    }

    public function findTablets() {
        return $this->getEntityManager->createQueryBuilder()
                        ->select('t')
                        ->from(\App\Entity\Hardware\Tablet::class, 't')
                        ->andWhere('t.isTablet = true')
                        ->getQuery()
                        ->getResult();
    }
}
